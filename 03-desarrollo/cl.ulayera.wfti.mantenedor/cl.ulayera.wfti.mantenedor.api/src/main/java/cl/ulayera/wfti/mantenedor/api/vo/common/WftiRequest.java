package cl.ulayera.wfti.mantenedor.api.vo.common;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ulayera on 11/11/14.
 */
public class WftiRequest implements Serializable {
    private Map<String, Object> argumentos;

    public WftiRequest() {
        argumentos = new HashMap<String, Object>();
    }

    public Object getParam(String paramName) {
        return argumentos.get(paramName);
    }

    public void addParam(String paramName, Object value) {
        argumentos.put(paramName, value);
    }

    public void deleteParam(String paramName) {
        argumentos.remove(paramName);
    }

    public void deleteAllParams() {
        argumentos.clear();
    }

    public <T extends Object> T getParam(String paramName, Class<T> type) {
        try {
            return type.cast(argumentos.get(paramName));
        } catch (Exception e) {
            assert true;
            return null;
        }
    }

    @Override
    public String toString() {
        return "WftiRequest{" +
                "argumentos=" + argumentos +
                '}';
    }
}
