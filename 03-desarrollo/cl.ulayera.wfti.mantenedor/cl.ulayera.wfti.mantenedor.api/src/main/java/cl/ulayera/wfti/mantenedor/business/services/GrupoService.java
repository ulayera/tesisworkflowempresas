package cl.ulayera.wfti.mantenedor.business.services;

import cl.ulayera.wfti.mantenedor.api.exceptions.WftiException;
import cl.ulayera.wfti.mantenedor.api.vo.common.WftiRequest;
import cl.ulayera.wfti.mantenedor.api.vo.common.WftiResponse;

/**
 * Creado por ulayera el 12/2/14.
 */
public interface GrupoService {
    WftiResponse findAllGrupos(WftiRequest request) throws WftiException;
    WftiResponse findGrupoById(WftiRequest request) throws WftiException;
    WftiResponse createGrupo(WftiRequest request) throws WftiException;
    WftiResponse deleteGrupo(WftiRequest request) throws WftiException;
    WftiResponse editGrupo(WftiRequest request) throws WftiException;
}
