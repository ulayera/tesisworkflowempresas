package cl.ulayera.wfti.mantenedor.api.vo.common;

import cl.ulayera.wfti.mantenedor.api.enums.CodigoSalidaEnum;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ulayera on 11/11/14.
 */
public class WftiResponse implements Serializable {
    private Map<String, Object> resultados;
    private CodigoSalidaEnum codigoSalida;
    private String mensajeSalida;

    public WftiResponse() {
        resultados = new HashMap<String, Object>();
    }

    public CodigoSalidaEnum getCodigoSalida() {
        return codigoSalida;
    }

    public void setCodigoSalida(CodigoSalidaEnum codigoSalida) {
        this.codigoSalida = codigoSalida;
    }

    public String getMensajeSalida() {
        return mensajeSalida;
    }

    public void setMensajeSalida(String mensajeSalida) {
        this.mensajeSalida = mensajeSalida;
    }

    public void addResultado(String key, Object value) {
        resultados.put(key, value);
    }

    public Object getResultado(String key){
        return resultados.get(key);
    }
}
