package cl.ulayera.wfti.mantenedor.api.vo.models;

import java.io.Serializable;

/**
 * Created by ulayera on 11/11/14.
 */
public class Rol implements Serializable {
    private Long id;
    private String nombre;
    private String nombreCorto;
    private String org_key;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombreCorto() {
        return nombreCorto;
    }

    public void setNombreCorto(String nombreCorto) {
        this.nombreCorto = nombreCorto;
    }

    public String getOrg_key() {
        return org_key;
    }

    public void setOrg_key(String org_key) {
        this.org_key = org_key;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Rol rol = (Rol) o;

        if (id != null ? !id.equals(rol.id) : rol.id != null) return false;
        if (nombre != null ? !nombre.equals(rol.nombre) : rol.nombre != null) return false;
        if (nombreCorto != null ? !nombreCorto.equals(rol.nombreCorto) : rol.nombreCorto != null) return false;
        if (org_key != null ? !org_key.equals(rol.org_key) : rol.org_key != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (nombre != null ? nombre.hashCode() : 0);
        result = 31 * result + (nombreCorto != null ? nombreCorto.hashCode() : 0);
        result = 31 * result + (org_key != null ? org_key.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Rol{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", nombreCorto='" + nombreCorto + '\'' +
                ", org_key='" + org_key + '\'' +
                '}';
    }
}
