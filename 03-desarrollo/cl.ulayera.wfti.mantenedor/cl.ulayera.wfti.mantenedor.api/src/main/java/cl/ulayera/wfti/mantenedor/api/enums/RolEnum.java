package cl.ulayera.wfti.mantenedor.api.enums;

/**
 * Creado por ulayera el 12/8/14.
 */
public enum RolEnum {
    USER, ADMIN;
}
