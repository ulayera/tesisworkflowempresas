package cl.ulayera.wfti.mantenedor.api.utils;

/**
 * Created by ulayera on 11/12/14.
 */

import org.apache.log4j.Logger;

import javax.naming.InitialContext;

public final class ServiceLocator {

    private static final Logger LOGGER = Logger.getLogger(ServiceLocator.class);
    private static ServiceLocator singleton;

    private ServiceLocator() {
    }

    public static ServiceLocator getInstance() {
        if (singleton == null) {
            singleton = new ServiceLocator();
        }
        return singleton;
    }

    public <T> T lookup(final String jndiName, Class<T> type) {
        try {
            InitialContext initialContext = new InitialContext();
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug(String.format("jndiName[%s]", jndiName));
            }
            return type.cast(initialContext.lookup(jndiName));
        } catch (Exception e) {
            LOGGER.fatal(e.getMessage(), e);
            return null;
        }
    }
}

