package cl.ulayera.wfti.mantenedor.api.utils;

import cl.ulayera.wfti.mantenedor.api.vo.Message;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ulayera on 11/12/14.
 */
public class TranslatorUtils {

    private static final Logger LOGGER = Logger.getLogger(TranslatorUtils.class);

    private TranslatorUtils() {
    }

    private static void addMessage(final String level, final Map<String, List<String>> translatedMap, Message message) {

        List<String> levelList = translatedMap.get(level);
        if (levelList == null) {
            levelList = new ArrayList<String>();
            translatedMap.put(level, levelList);
            LOGGER.debug(level + "");
        }

        levelList.add(message.getCode());
    }

    public static Map<String, List<String>> translate(final List<Message> messageList) {

        Map<String, List<String>> translatedMap = new HashMap<String, List<String>>();

        if (messageList != null) {
            for (Message message : messageList) {
                if (message.getType() != null) {

                    switch (message.getType()) {

                        case INFO:
                            addMessage(Parameters.INFO, translatedMap, message);
                            break;
                        case WARN:
                            addMessage(Parameters.WARN, translatedMap, message);
                            break;
                        case ERROR:
                            addMessage(Parameters.ERROR, translatedMap, message);
                            break;
                        default:
                            addMessage(Parameters.INFO, translatedMap, message);
                    }
                }
            }
        }

        return translatedMap;
    }
}
