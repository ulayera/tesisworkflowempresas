package cl.ulayera.wfti.mantenedor.api.vo.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ulayera on 11/11/14.
 */
public class Proyecto implements Serializable {
    private Long id;
    private String nombre;
    private List<Long> actividades = new ArrayList<Long>();
    private List<Integer> ordenes = new ArrayList<Integer>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<Long> getActividades() {
        return actividades;
    }

    public void setActividades(List<Long> actividades) {
        this.actividades = actividades;
    }

    public List<Integer> getOrdenes() {
        return ordenes;
    }

    public void setOrdenes(List<Integer> ordenes) {
        this.ordenes = ordenes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Proyecto proyecto = (Proyecto) o;

        if (id != null ? !id.equals(proyecto.id) : proyecto.id != null) return false;
        if (nombre != null ? !nombre.equals(proyecto.nombre) : proyecto.nombre != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (nombre != null ? nombre.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Proyecto{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                '}';
    }
}
