package cl.ulayera.wfti.mantenedor.business.services;

import cl.ulayera.wfti.mantenedor.api.exceptions.WftiException;
import cl.ulayera.wfti.mantenedor.api.vo.common.WftiRequest;
import cl.ulayera.wfti.mantenedor.api.vo.common.WftiResponse;

/**
 * Creado por ulayera el 12/2/14.
 */
public interface ProyectoService {
    WftiResponse findAllProyectos(WftiRequest request) throws WftiException;
    WftiResponse findProyectoById(WftiRequest request) throws WftiException;
    WftiResponse createProyecto(WftiRequest request) throws WftiException;
    WftiResponse deleteProyecto(WftiRequest request) throws WftiException;
    WftiResponse updateProyecto(WftiRequest request) throws WftiException;
}
