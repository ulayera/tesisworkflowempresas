package cl.ulayera.wfti.mantenedor.api.exceptions;

import cl.ulayera.wfti.mantenedor.api.vo.common.WftiRequest;

/**
 * Created by ulayera on 11/22/14.
 */
public class WftiException extends Exception{
    private String businessMessage;
    private WftiRequest wftiRequest;

    public WftiException() {
        super();
    }

    public WftiException(String businessMessage) {
        super();
        this.businessMessage = businessMessage;
    }

    public WftiException(Throwable cause, String businessMessage) {
        super(cause);
        this.businessMessage = businessMessage;
    }

    public String getBusinessMessage() {
        return businessMessage;
    }

    public WftiRequest getWftiRequest() {
        return wftiRequest;
    }

    public void setWftiRequest(WftiRequest wftiRequest) {
        this.wftiRequest = wftiRequest;
    }

    @Override
    public String toString() {
        return "WftiException{" +
                "businessMessage='" + businessMessage + '\'' +
                ", wftiRequest=" + wftiRequest +
                '}';
    }
}
