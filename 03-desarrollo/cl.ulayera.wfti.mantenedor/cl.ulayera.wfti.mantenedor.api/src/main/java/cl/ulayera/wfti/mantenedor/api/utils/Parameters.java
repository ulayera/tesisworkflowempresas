package cl.ulayera.wfti.mantenedor.api.utils;

/**
 * Created by ulayera on 11/12/14.
 */
public interface Parameters {
    String INFO = "info";
    String WARN = "warn";
    String ERROR = "error";
}
