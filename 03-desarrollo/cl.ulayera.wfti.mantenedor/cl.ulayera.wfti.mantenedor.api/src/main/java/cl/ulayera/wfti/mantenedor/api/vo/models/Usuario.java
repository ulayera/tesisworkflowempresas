package cl.ulayera.wfti.mantenedor.api.vo.models;

import cl.ulayera.wfti.mantenedor.api.enums.RolEnum;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Creado por ulayera el 11/11/14.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Usuario implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
    private String username;
    private String nombre;
    private String password;
    private String org_key;
    private String locale;
    private String rol;
    private List<Long> idGrupos;

    public Usuario() {
        idGrupos = new ArrayList<Long>();
    }

    @Override
    public String toString() {
        return "Usuario{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", nombre='" + nombre + '\'' +
                ", password='" + password + '\'' +
                ", org_key='" + org_key + '\'' +
                ", locale='" + locale + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Usuario usuario1 = (Usuario) o;

        if (id != null ? !id.equals(usuario1.id) : usuario1.id != null) return false;
        if (locale != null ? !locale.equals(usuario1.locale) : usuario1.locale != null) return false;
        if (nombre != null ? !nombre.equals(usuario1.nombre) : usuario1.nombre != null) return false;
        if (org_key != null ? !org_key.equals(usuario1.org_key) : usuario1.org_key != null) return false;
        if (password != null ? !password.equals(usuario1.password) : usuario1.password != null) return false;
        if (username != null ? !username.equals(usuario1.username) : usuario1.username != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (nombre != null ? nombre.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (org_key != null ? org_key.hashCode() : 0);
        result = 31 * result + (locale != null ? locale.hashCode() : 0);
        return result;
    }

    public Long getId() {

        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getOrg_key() {
        return org_key;
    }

    public void setOrg_key(String org_key) {
        this.org_key = org_key;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public List<Long> getIdGrupos() {
        return idGrupos;
    }

    public void setIdGrupos(List<Long> idGrupos) {
        this.idGrupos = idGrupos;
    }
}