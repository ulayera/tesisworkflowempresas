package cl.ulayera.wfti.mantenedor.api.enums;

/**
 * Created by ulayera on 11/12/14.
 */
public enum MessageType {
    INFO,
    WARN,
    ERROR;
}
