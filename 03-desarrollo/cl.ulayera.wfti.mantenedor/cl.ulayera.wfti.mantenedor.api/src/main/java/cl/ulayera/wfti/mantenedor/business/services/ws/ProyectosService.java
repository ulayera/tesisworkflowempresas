package cl.ulayera.wfti.mantenedor.business.services.ws;

import cl.ulayera.wfti.mantenedor.api.schemas.ProyectoSchema;

import java.util.List;

/**
 * Creado por ulayera el 12/14/14.
 */
public interface ProyectosService {
    List<ProyectoSchema> consultaProyectos();
}
