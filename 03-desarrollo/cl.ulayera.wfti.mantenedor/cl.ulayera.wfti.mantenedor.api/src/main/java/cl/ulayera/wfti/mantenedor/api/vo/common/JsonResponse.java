package cl.ulayera.wfti.mantenedor.api.vo.common;

import cl.ulayera.wfti.mantenedor.api.utils.Parameters;
import cl.ulayera.wfti.mantenedor.api.utils.TranslatorUtils;
import cl.ulayera.wfti.mantenedor.api.vo.Message;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by ulayera on 11/12/14.
 */
public class JsonResponse implements Serializable {

    private Boolean success = false;
    private Object body = "empty";
    private List<String> mensajesInfo = new ArrayList<String>();
    private List<String> mensajesWarning = new ArrayList<String>();
    private List<String> mensajesError = new ArrayList<String>();
    private String urlRedirect = null;
    private Boolean requiereConfirmacion = Boolean.FALSE;

    public JsonResponse() {
    }

    public Boolean getRequiereConfirmacion() {
        return requiereConfirmacion;
    }

    public void setRequiereConfirmacion(Boolean requiereConfirmacion) {
        this.requiereConfirmacion = requiereConfirmacion;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public void setSuccessToTrue() {
        success = Boolean.TRUE;
    }

    public void setSuccessToFalse() {
        success = Boolean.FALSE;
    }

    public Object getBody() {
        return body;
    }

    public void setBody(Object body) {
        this.body = body;
    }

    public String getUrlRedirect() {
        return urlRedirect;
    }

    public void setUrlRedirect(String urlRedirect) {
        this.urlRedirect = urlRedirect;
    }

    public List<String> getMensajesInfo() {
        return mensajesInfo;
    }

    public void setMensajesInfo(List<String> mensajesInfo) {
        this.mensajesInfo = mensajesInfo;
    }

    public void addMensajeInfo(String mensaje) {
        if (mensajesInfo == null) {
            mensajesInfo = new ArrayList<String>();
        }

        mensajesInfo.add(mensaje);
    }

    public List<String> getMensajesWarning() {
        return mensajesWarning;
    }

    public void setMensajesWarning(List<String> mensajesWarning) {
        this.mensajesWarning = mensajesWarning;
    }

    public void addMensajeWarning(String warning) {
        if (mensajesWarning == null) {
            mensajesWarning = new ArrayList<String>();
        }

        mensajesWarning.add(warning);
    }

    public List<String> getMensajesError() {
        return mensajesError;
    }

    public void setMensajesError(List<String> mensajesError) {
        this.mensajesError = mensajesError;
    }

    public void addMensajeError(String error) {
        if (mensajesError == null) {
            mensajesError = new ArrayList<String>();
        }

        mensajesError.add(error);
    }

    public void addAll(Map<String, List<String>> mapMensajes) {

        if (mapMensajes.get(Parameters.INFO) != null) {

            if (mensajesInfo == null) {
                mensajesInfo = new ArrayList<String>();
            }

            mensajesInfo.addAll(mapMensajes.get(Parameters.INFO));
        }

        if (mapMensajes.get(Parameters.WARN) != null) {
            if (mensajesWarning == null) {
                mensajesWarning = new ArrayList<String>();
            }

            mensajesWarning.addAll(mapMensajes.get(Parameters.WARN));
        }

        if (mapMensajes.get(Parameters.ERROR) != null) {
            if (mensajesError == null) {
                mensajesError = new ArrayList<String>();
            }

            mensajesError.addAll(mapMensajes.get(Parameters.ERROR));
        }
    }

    public void addAllMessages(final List<Message> messages) {
        addAll(TranslatorUtils.translate(messages));
    }

    public void addAllMessagesError(final List<String> messages) {
        mensajesError = messages;
    }

    @Override
    public String toString() {
        return "JsonResponse{" +
                "success=" + success +
                ", body=" + body +
                ", mensajesInfo=" + mensajesInfo +
                ", mensajesWarning=" + mensajesWarning +
                ", mensajesError=" + mensajesError +
                ", urlRedirect='" + urlRedirect + '\'' +
                '}';
    }


}

