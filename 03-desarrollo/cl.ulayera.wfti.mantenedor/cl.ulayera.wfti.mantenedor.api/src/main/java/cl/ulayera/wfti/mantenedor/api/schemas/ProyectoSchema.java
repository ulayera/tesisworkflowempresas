package cl.ulayera.wfti.mantenedor.api.schemas;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

/**
 * Creado por ulayera el 12/14/14.
 */
@XmlType(propOrder={"id", "nombre"})
public class ProyectoSchema {
    @XmlAttribute
    private Long id;
    private String nombre;
}
