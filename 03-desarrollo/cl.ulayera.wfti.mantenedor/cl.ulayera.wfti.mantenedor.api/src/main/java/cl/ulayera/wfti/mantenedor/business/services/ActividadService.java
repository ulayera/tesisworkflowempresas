package cl.ulayera.wfti.mantenedor.business.services;

import cl.ulayera.wfti.mantenedor.api.exceptions.WftiException;
import cl.ulayera.wfti.mantenedor.api.vo.common.WftiRequest;
import cl.ulayera.wfti.mantenedor.api.vo.common.WftiResponse;

/**
 * Creado por ulayera el 12/2/14.
 */
public interface ActividadService {
    WftiResponse findAllActividades(WftiRequest request) throws WftiException;
    WftiResponse findActividadById(WftiRequest request) throws WftiException;
    WftiResponse createActividad(WftiRequest request) throws WftiException;
    WftiResponse deleteActividad(WftiRequest request) throws WftiException;
    WftiResponse updateActividad(WftiRequest request) throws WftiException;

}
