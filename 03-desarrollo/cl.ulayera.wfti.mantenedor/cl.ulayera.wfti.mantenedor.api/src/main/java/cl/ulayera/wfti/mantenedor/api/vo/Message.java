package cl.ulayera.wfti.mantenedor.api.vo;

import cl.ulayera.wfti.mantenedor.api.enums.MessageType;

import java.io.Serializable;

/**
 * Created by ulayera on 11/12/14.
 */
public class Message implements Serializable {

    private static final long serialVersionUID = 1l;
    private MessageType type;
    private String code;

    public Message() {
    }

    public Message(MessageType type, String code) {
        this.type = type;
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public MessageType getType() {
        return type;
    }

    public void setType(MessageType type) {
        this.type = type;
    }
}

