package cl.ulayera.wfti.mantenedor.api.vo.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ulayera on 11/11/14.
 */
public class Actividad implements Serializable {
    private Long id;
    private String nombre;
    private String datos;
    private String descripcion;
    private List<Long> idGrupos = new ArrayList<Long>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDatos() {
        return datos;
    }

    public void setDatos(String datos) {
        this.datos = datos;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion == null ? "" : descripcion;
    }

    public List<Long> getIdGrupos() {
        return idGrupos;
    }

    public void setIdGrupos(List<Long> idGrupos) {
        this.idGrupos = idGrupos;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Actividad actividad = (Actividad) o;

        if (datos != null ? !datos.equals(actividad.datos) : actividad.datos != null) return false;
        if (descripcion != null ? !descripcion.equals(actividad.descripcion) : actividad.descripcion != null)
            return false;
        if (id != null ? !id.equals(actividad.id) : actividad.id != null) return false;
        if (nombre != null ? !nombre.equals(actividad.nombre) : actividad.nombre != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (nombre != null ? nombre.hashCode() : 0);
        result = 31 * result + (datos != null ? datos.hashCode() : 0);
        result = 31 * result + (descripcion != null ? descripcion.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Actividad{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", datos='" + datos + '\'' +
                ", descripcion='" + descripcion + '\'' +
                '}';
    }
}
