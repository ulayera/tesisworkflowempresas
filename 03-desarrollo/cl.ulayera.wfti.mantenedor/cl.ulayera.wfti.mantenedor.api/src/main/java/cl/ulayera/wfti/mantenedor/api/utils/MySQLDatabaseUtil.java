package cl.ulayera.wfti.mantenedor.api.utils;

import cl.ulayera.wfti.mantenedor.api.exceptions.WftiException;
import cl.ulayera.wfti.mantenedor.api.vo.models.Actividad;
import cl.ulayera.wfti.mantenedor.api.vo.models.Grupo;
import cl.ulayera.wfti.mantenedor.api.vo.models.Proyecto;
import cl.ulayera.wfti.mantenedor.api.vo.models.Usuario;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Creado por ulayera el 11/11/14.
 */
public class MySQLDatabaseUtil {
    private static final Logger LOGGER = Logger.getLogger(MySQLDatabaseUtil.class);
    private static final String connectionURI = "jdbc:mysql://localhost:3306/wfti?user=root&password=mypass";

    public static Usuario findUsuarioByUsername(String username) throws WftiException {
        Usuario resultado = null;
        String query = String.format("SELECT * FROM wf_usuario where username = '%s'", username);
        Connection connect = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");

            connect = DriverManager
                    .getConnection(connectionURI);

            Statement statement = connect.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            LOGGER.debug("Query executed: " + query);
            if (resultSet.next()) {
                resultado = usuarioFromResultSet(resultSet);
            }
        } catch (Exception e) {
            LOGGER.error(e);
            throw new WftiException(e, "Error al ejecutar: " + new Object() {
            }.getClass().getEnclosingMethod().getName());
        } finally {
            try {
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException e) {
                LOGGER.error(e);
            }
        }

        return resultado;
    }

    public static List<Usuario> findAllUsuarios() throws WftiException {
        List<Usuario> usuarioList = new ArrayList<Usuario>();

        String query = "SELECT * FROM wf_usuario ";
        Connection connect = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");

            connect = DriverManager
                    .getConnection(connectionURI);

            Statement statement = connect.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            LOGGER.debug("Query executed: " + query);
            while (resultSet.next()) {
                usuarioList.add(usuarioFromResultSet(resultSet));

            }
        } catch (Exception e) {
            LOGGER.error(e);
            throw new WftiException(e, "Error al ejecutar: " + new Object() {
            }.getClass().getEnclosingMethod().getName());
        } finally {
            try {
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException e) {
                LOGGER.error(e);
            }
        }

        return usuarioList;
    }

    public static List<Grupo> findAllGrupos() throws WftiException {
        List<Grupo> grupoList = new ArrayList<Grupo>();

        String query = "SELECT * FROM wf_grupo ";
        Connection connect = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");

            connect = DriverManager
                    .getConnection(connectionURI);

            Statement statement = connect.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            LOGGER.debug("Query executed: " + query);
            while (resultSet.next()) {
                grupoList.add(grupoFromResultSet(resultSet));

            }
        } catch (Exception e) {
            LOGGER.error(e);
            throw new WftiException(e, "Error al ejecutar: " + new Object() {
            }.getClass().getEnclosingMethod().getName());
        } finally {
            try {
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException e) {
                LOGGER.error(e);
            }
        }

        return grupoList;
    }

    public static List<Actividad> findAllActividades() throws WftiException {
        List<Actividad> actividadList = new ArrayList<Actividad>();

        String query = "SELECT * FROM wf_actividad ";
        Connection connect = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");

            connect = DriverManager
                    .getConnection(connectionURI);

            Statement statement = connect.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            LOGGER.debug("Query executed: " + query);
            while (resultSet.next()) {
                actividadList.add(actividadFromResultSet(resultSet));

            }
        } catch (Exception e) {
            LOGGER.error(e);
            throw new WftiException(e, "Error al ejecutar: " + new Object() {
            }.getClass().getEnclosingMethod().getName());
        } finally {
            try {
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException e) {
                LOGGER.error(e);
            }
        }

        return actividadList;
    }

    public static List<Proyecto> findAllProyectos() throws WftiException {
        List<Proyecto> proyectoList = new ArrayList<Proyecto>();

        String query = "SELECT * FROM wf_proyecto ";
        Connection connect = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");

            connect = DriverManager
                    .getConnection(connectionURI);

            Statement statement = connect.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            LOGGER.debug("Query executed: " + query);
            while (resultSet.next()) {
                proyectoList.add(proyectoFromResultSet(resultSet));

            }
        } catch (Exception e) {
            LOGGER.error(e);
            throw new WftiException(e, "Error al ejecutar: " + new Object() {
            }.getClass().getEnclosingMethod().getName());
        } finally {
            try {
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException e) {
                LOGGER.error(e);
            }
        }

        return proyectoList;
    }


    public static Usuario findUsuarioById(Long id) throws WftiException {
        Usuario resultado = null;
        String query = String.format("SELECT * FROM wf_usuario where id = %d", id);
        Connection connect = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");

            connect = DriverManager
                    .getConnection(connectionURI);

            Statement statement = connect.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            LOGGER.debug("Query executed: " + query);
            if (resultSet.next()) {
                resultado = usuarioFromResultSet(resultSet);
                String query2 = String.format("SELECT * FROM WF_USUARIO_GRUPO WHERE id_usuario = (%d)",
                        resultado.getId());
                ResultSet resultSet2 = statement.executeQuery(query2);
                while (resultSet2.next()) {
                    Long idGrupo = resultSet2.getLong("id_grupo");
                    resultado.getIdGrupos().add(idGrupo);
                }
            }
        } catch (Exception e) {
            LOGGER.error(e);
            throw new WftiException(e, "Error al ejecutar: " + new Object() {
            }.getClass().getEnclosingMethod().getName());
        } finally {
            try {
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException e) {
                LOGGER.error(e);
            }
        }

        return resultado;
    }

    public static Grupo findGrupoById(Long id) throws WftiException {
        Grupo resultado = null;
        String query = String.format("SELECT * FROM wf_grupo where id = %d", id);
        Connection connect = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");

            connect = DriverManager
                    .getConnection(connectionURI);

            Statement statement = connect.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            LOGGER.debug("Query executed: " + query);
            if (resultSet.next()) {
                resultado = grupoFromResultSet(resultSet);
            }
        } catch (Exception e) {
            LOGGER.error(e);
            throw new WftiException(e, "Error al ejecutar: " + new Object() {
            }.getClass().getEnclosingMethod().getName());
        } finally {
            try {
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException e) {
                LOGGER.error(e);
            }
        }

        return resultado;
    }

    public static Actividad findActividadById(Long id) throws WftiException {
        Actividad resultado = null;
        String query = String.format("SELECT * FROM wf_actividad where id = %d", id);
        Connection connect = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");

            connect = DriverManager
                    .getConnection(connectionURI);

            Statement statement = connect.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            LOGGER.debug("Query executed: " + query);
            if (resultSet.next()) {
                resultado = actividadFromResultSet(resultSet);
                String query2 = String.format("SELECT * FROM wf_actividad_grupo WHERE id_actividad = (%d)",
                        resultado.getId());
                ResultSet resultSet2 = statement.executeQuery(query2);
                while (resultSet2.next()) {
                    Long idGrupo = resultSet2.getLong("id_grupo");
                    resultado.getIdGrupos().add(idGrupo);
                }
            }
        } catch (Exception e) {
            LOGGER.error(e);
            throw new WftiException(e, "Error al ejecutar: " + new Object() {
            }.getClass().getEnclosingMethod().getName());
        } finally {
            try {
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException e) {
                LOGGER.error(e);
            }
        }

        return resultado;
    }

    public static Proyecto findProyectoById(Long id) throws WftiException {
        Proyecto resultado = null;
        String query = String.format("SELECT * FROM wf_proyecto where id = %d", id);
        Connection connect = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");

            connect = DriverManager
                    .getConnection(connectionURI);

            Statement statement = connect.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            LOGGER.debug("Query executed: " + query);
            if (resultSet.next()) {
                resultado = proyectoFromResultSet(resultSet);
                String query2 = String.format("SELECT * FROM wfti.wf_proyecto_actividad WHERE ID_PROYECTO = (%d)",
                        resultado.getId());
                ResultSet resultSet2 = statement.executeQuery(query2);
                while (resultSet2.next()) {
                    Long idActividad = resultSet2.getLong("ID_ACTIVIDAD");
                    Integer orden = resultSet2.getInt("POSICION");
                    resultado.getActividades().add(idActividad);
                    resultado.getOrdenes().add(orden);
                }
            }
        } catch (Exception e) {
            LOGGER.error(e);
            throw new WftiException(e, "Error al ejecutar: " + new Object() {
            }.getClass().getEnclosingMethod().getName());
        } finally {
            try {
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException e) {
                LOGGER.error(e);
            }
        }

        return resultado;
    }

    public static Usuario createUsuario(Usuario usuario) throws WftiException {
        Boolean ok = true;

        if (findUsuarioByUsername(usuario.getUsername()) == null) {
            String query = String.format("INSERT INTO wf_usuario " +
                            "(username,org_key,nombre,password,locale) values ('%s','%s','%s','%s','%s') ",
                    usuario.getUsername(),
                    usuario.getOrg_key(),
                    usuario.getNombre(),
                    usuario.getPassword(),
                    usuario.getLocale());
            Long id = executeInsert(query);
            usuario = findUsuarioByUsername(usuario.getUsername());

            for (Long idGrupo : usuario.getIdGrupos()) {
                String query3 = String.format("INSERT INTO WF_USUARIO_GRUPO (id_grupo,id_usuario) values ('%d','%d') ",
                        idGrupo,
                        usuario.getId());
                ok = executeInsert(query3) != null;
            }

            Long idRol  = ("admin".equals(usuario.getRol())) ? 2L : 1L;

            String query2 = String.format("DELETE FROM wf_rol_usuario WHERE id_usuario = %d "
                    ,usuario.getId());
            executeDelete(query2);

            String query3 = String.format("INSERT INTO wf_rol_usuario (ID_USUARIO, ID_ROL) VALUES ('%d', '%d') "
                    ,usuario.getId(), idRol);
            executeInsert(query3);

            if (id != null && ok) {
                usuario.setId(id);
            } else {
                usuario = null;
            }
        } else {
            throw new WftiException("El username ingresado ya existe, intente con otro.");
        }

        return usuario;
    }

    public static Grupo createGrupo(Grupo grupo) throws WftiException {
        String query = String.format("INSERT INTO wf_grupo (nombre) values ('%s')",
                grupo.getNombre());
        Long id = executeInsert(query);
        if (id != null) {
            grupo.setId(id);
        } else {
            grupo = null;
        }
        return grupo;
    }

    public static Actividad createActividad(Actividad actividad) throws WftiException {
        String query = String.format("INSERT INTO wf_actividad (nombre,datos,descripcion) values ('%s','%s','%s')",
                actividad.getNombre(),
                actividad.getDatos(),
                actividad.getDescripcion());
        Long id = executeInsert(query);
        if (id != null) {
            actividad.setId(id);
            createGruposActividad(actividad);
        } else {
            actividad = null;
        }
        return actividad;
    }

    public static Actividad updateActividad(Actividad actividad) throws WftiException {
        String query = String.format("UPDATE wfti.wf_actividad SET NOMBRE = '%s', DESCRIPCION = '%s' WHERE ID = '%d';",
                actividad.getNombre(), actividad.getDescripcion(),actividad.getId());
        executeUpdate(query);
        createGruposActividad(actividad);
        return actividad;
    }

    private static void deleteGruposActividad(Actividad actividad) throws WftiException {
        String query = String.format("DELETE FROM wfti.wf_actividad_grupo WHERE ID_ACTIVIDAD='%d'" ,actividad.getId());
        executeDelete(query);
    }
    private static void createGruposActividad(Actividad actividad) throws WftiException  {
        deleteGruposActividad(actividad);
        for(Long idGrupo : actividad.getIdGrupos()) {
            String query = String.format("INSERT INTO wfti.wf_actividad_grupo (ID_ACTIVIDAD, ID_GRUPO) VALUES ('%d', '%d')"
                    ,actividad.getId(), idGrupo);
            executeInsert(query);
        }
    }

    public static Proyecto createProyecto(Proyecto proyecto) throws WftiException {
        String query = String.format("INSERT INTO wf_proyecto (nombre) values ('%s')",
                proyecto.getNombre());
        Long id = executeInsert(query);
        if (id != null) {
            proyecto.setId(id);
            createActividadesProyecto(proyecto);
        } else {
            proyecto = null;
        }
        return proyecto;
    }

    public static Proyecto updateProyecto(Proyecto proyecto) throws WftiException {
        String query = String.format("UPDATE wfti.wf_proyecto SET NOMBRE = '%s' WHERE ID = '%d';",
                proyecto.getNombre(),proyecto.getId());
        executeUpdate(query);
        createActividadesProyecto(proyecto);
        return proyecto;
    }

    private static void deleteActividadesProyecto(Proyecto proyecto) throws WftiException {
        String query = String.format("DELETE FROM wfti.wf_proyecto_actividad WHERE ID_PROYECTO='%d'" ,proyecto.getId());
        executeDelete(query);
    }
    private static void createActividadesProyecto(Proyecto proyecto) throws WftiException  {
        deleteActividadesProyecto(proyecto);
        for(int i = 0; i < proyecto.getActividades().size() ; i++) {
            Long idActividad = proyecto.getActividades().get(i);
            Integer orden = proyecto.getOrdenes().get(i);
            String query = String.format("INSERT INTO wfti.wf_proyecto_actividad (ID_PROYECTO, ID_ACTIVIDAD, posicion) VALUES ('%d', '%d', '%d')"
                    ,proyecto.getId(), idActividad, orden);
            executeInsert(query);
        }
    }

    public static Boolean deleteUsuario(Usuario usuario) throws WftiException {
        String query = String.format("DELETE FROM wf_usuario " +
                        "WHERE id = (%d)",
                usuario.getId());
        return executeDelete(query);
    }

    public static Boolean deleteGrupo(Grupo grupo) throws WftiException {
        String query = String.format("DELETE FROM wf_grupo WHERE id = (%d)",
                grupo.getId());
        return executeDelete(query);
    }

    public static Boolean deleteActividad(Actividad actividad) throws WftiException {
        String query = String.format("DELETE FROM wf_actividad WHERE id = (%d)",
                actividad.getId());
        deleteGruposActividad(actividad);
        return executeDelete(query);
    }

    public static Boolean deleteProyecto(Proyecto proyecto) throws WftiException {
        String query = String.format("DELETE FROM wf_proyecto WHERE id = (%d)",
                proyecto.getId());
        deleteActividadesProyecto(proyecto);
        return executeDelete(query);
    }

    public static Boolean editUsuario(Usuario usuario) throws WftiException {
        Boolean ok;
        String query = String.format("UPDATE wf_usuario SET username='%s',nombre='%s',password='%s' WHERE id = (%d)",
                usuario.getUsername(),
                usuario.getNombre(),
                usuario.getPassword(),
                usuario.getId());
        ok = executeUpdate(query);
        if (ok) {
            String query2 = String.format("DELETE FROM WF_USUARIO_GRUPO WHERE id_usuario = (%d)",
                    usuario.getId());
            ok = executeDelete(query2);
            for (Long idGrupo : usuario.getIdGrupos()) {
                if (ok) {
                    String query3 = String.format("INSERT INTO WF_USUARIO_GRUPO (id_grupo,id_usuario) values ('%d','%d')",
                            idGrupo,
                            usuario.getId());
                    ok = executeInsert(query3) != null;
                }
            }
        }
        return ok;
    }

    private static Long executeInsert(String query) throws WftiException {
        Long id = null;
        Connection connect = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");

            connect = DriverManager
                    .getConnection(connectionURI);

            Statement statement = connect.createStatement();
            int affectedRows = statement.executeUpdate(query,Statement.RETURN_GENERATED_KEYS);

            if (affectedRows == 0) {
                throw new SQLException("Fallo la creacion, 0 filas afectadas");
            }
            ResultSet generatedKeys = statement.getGeneratedKeys();
            if (generatedKeys.next()) {
                id = generatedKeys.getLong(1);
            } else {
                throw new SQLException("Fallo la creacion, no se obtuvo id");
            }

            LOGGER.debug("Query executed: " + query);
        } catch (Exception e) {
            LOGGER.error(e);
            throw new WftiException(e, "Error al ejecutar: " + new Object() {
            }.getClass().getEnclosingMethod().getName() + ": " + e.getMessage());
        } finally {
            try {
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException e) {
                LOGGER.error(e);
            }
        }
        return id;
    }

    private static Boolean executeUpdate(String query) throws WftiException {
        Boolean ok = false;
        Connection connect = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");

            connect = DriverManager
                    .getConnection(connectionURI);

            Statement statement = connect.createStatement();
            int affectedRows = statement.executeUpdate(query);

            if (affectedRows == 0) {
                throw new SQLException("Fallo la edicion, 0 filas afectadas");
            }

            LOGGER.debug("Query executed: " + query);
            ok = true;
        } catch (Exception e) {
            LOGGER.error(e);
            throw new WftiException(e, "Error al ejecutar: " + new Object() {
            }.getClass().getEnclosingMethod().getName() + ": " + e.getMessage());
        } finally {
            try {
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException e) {
                LOGGER.error(e);
            }
        }
        return ok;
    }

    private static Boolean executeDelete(String query) throws WftiException {
        Boolean ok = false;
        Connection connect = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");

            connect = DriverManager
                    .getConnection(connectionURI);

            Statement statement = connect.createStatement();
            statement.executeUpdate(query);
            LOGGER.debug("Query executed: " + query);
            ok = true;
        } catch (Exception e) {
            LOGGER.error(e);
            throw new WftiException(e, "Error al ejecutar: " + new Object() {
            }.getClass().getEnclosingMethod().getName() + ": " + e.getMessage());
        } finally {
            try {
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException e) {
                LOGGER.error(e);
            }
        }
        return ok;
    }

    private static Usuario usuarioFromResultSet(ResultSet resultSet) {
        Usuario actual = new Usuario();
        try {
            actual.setId(resultSet.getLong("id"));
            actual.setUsername(resultSet.getString("username"));
            actual.setOrg_key(resultSet.getString("org_key"));
            actual.setNombre(resultSet.getString("nombre"));
            actual.setPassword(resultSet.getString("password"));
            actual.setLocale(resultSet.getString("locale"));
        } catch (SQLException e) {
            LOGGER.error(e);
        }
        return actual;
    }

    private static Grupo grupoFromResultSet(ResultSet resultSet) {
        Grupo actual = new Grupo();
        try {
            actual.setId(resultSet.getLong("id"));
            actual.setNombre(resultSet.getString("nombre"));
        } catch (SQLException e) {
            LOGGER.error(e);
        }
        return actual;
    }

    private static Actividad actividadFromResultSet(ResultSet resultSet) {
        Actividad actual = new Actividad();
        try {
            actual.setId(resultSet.getLong("id"));
            actual.setNombre(resultSet.getString("nombre"));
            actual.setDescripcion(resultSet.getString("descripcion"));
            actual.setDatos(resultSet.getString("datos"));
        } catch (SQLException e) {
            LOGGER.error(e);
        }
        return actual;
    }

    private static Proyecto proyectoFromResultSet(ResultSet resultSet) {
        Proyecto actual = new Proyecto();
        try {
            actual.setId(resultSet.getLong("id"));
            actual.setNombre(resultSet.getString("nombre"));
        } catch (SQLException e) {
            LOGGER.error(e);
        }
        return actual;
    }

    public static Boolean editGrupo(Grupo grupo) throws WftiException {
        String query = String.format("UPDATE wf_grupo SET NOMBRE = '%s' WHERE ID = '%d';",
                grupo.getNombre(),grupo.getId());
        return executeUpdate(query);
    }
}
