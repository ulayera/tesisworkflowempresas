package cl.ulayera.wfti.mantenedor.business.services;

import cl.ulayera.wfti.mantenedor.api.exceptions.WftiException;
import cl.ulayera.wfti.mantenedor.api.vo.common.WftiRequest;
import cl.ulayera.wfti.mantenedor.api.vo.common.WftiResponse;

/**
 * Created by ulayera on 11/11/14.
 */
public interface LoginService {
    WftiResponse intentaLogin(WftiRequest request) throws WftiException;
    WftiResponse validaSesion(WftiRequest request);
}
