package cl.ulayera.wfti.mantenedor.business.services;

import cl.ulayera.wfti.mantenedor.api.exceptions.WftiException;
import cl.ulayera.wfti.mantenedor.api.vo.common.WftiRequest;
import cl.ulayera.wfti.mantenedor.api.vo.common.WftiResponse;

/**
 * Creado por ulayera el 12/2/14.
 */
public interface UsuarioService {
    WftiResponse findAllUsuarios(WftiRequest request) throws WftiException;
    WftiResponse findUsuarioById(WftiRequest request) throws WftiException;
    WftiResponse createUsuario(WftiRequest request) throws WftiException;
    WftiResponse deleteUsuario(WftiRequest request) throws WftiException;
    WftiResponse editUsuario(WftiRequest request) throws WftiException;
}
