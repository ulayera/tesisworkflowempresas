<%--
  Created by IntelliJ IDEA.
  User: ulayera
  Date: 11/11/14
  Time: 3:36 AM
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <jsp:include page="common/scripts-view-section.jsp"/>
    <script src="http://crypto-js.googlecode.com/svn/tags/3.1.2/build/rollups/sha1.js"></script>
    <title>WFTI :: Mantenedor :: Actividades</title>
</head>
<body>
<jsp:include page="common/menu-view.jsp"/>

<div class="row well">
    <div class="col-lg-6">
        <div class="row top-buffer">
            <div class="col-lg-6">
                <label for="id">ID de la Actividad</label>
            </div>
            <div class="col-lg-6">
                <input id="id" type="text" style="width: 100%" disabled/>
            </div>
        </div>
        <div class="row top-buffer">
            <div class="col-lg-6">
                <label for="nombre">Nombre de la Actividad</label>
            </div>
            <div class="col-lg-6">
                <input id="nombre" type="text" style="width: 100%"/>
            </div>
        </div>
        <div class="row top-buffer">
            <div class="col-lg-6">
                <label for="descripcion">Descripcion </label>
            </div>
            <div class="col-lg-6">
                <textarea id="descripcion" type="text" style="width: 100%; height: 150px; resize: none;"></textarea>
            </div>
        </div>

        <div class="row top-buffer">
            <div class="col-lg-6">
            </div>
            <div class="col-lg-6" id="divBotones">
                <button id="btnCrear">Crear</button>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <table class="table">
            <thead>
            <tr>
                <th>Grupos</th>
                <th>Pertenece a</th>
            </tr>
            </thead>
            <tbody id="listaGruposActividad">
            <!--
            <tr>
                <td>Grupo ejemplo 1</td>
                <td><input type="checkbox"></td>
            </tr>
            <tr>
                <td>Grupo ejemplo 2</td>
                <td><input type="checkbox" checked></td>
            </tr>
            -->
            </tbody>
        </table>
    </div>
</div>

<div class="row well">
    <div class="col-lg-12">
        <table class="table">
            <thead>
            <tr>
                <th>ID de la Actividad</th>
                <th>Nombre de la Actividad</th>
                <th>Eliminar</th>
                <th>Editar</th>
            </tr>
            </thead>
            <tbody id="listaActividades">

            </tbody>
        </table>
    </div>
</div>

</body>
<script src="/mantenedor/js/view-js/actividades-view.js" type="application/javascript"></script>
<jsp:include page="common/footer-includes-view-section.jsp"/>
</html>