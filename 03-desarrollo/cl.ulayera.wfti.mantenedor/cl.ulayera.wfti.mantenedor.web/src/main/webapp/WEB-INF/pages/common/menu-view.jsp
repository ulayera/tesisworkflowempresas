<br>
<div class="container" style="width: 98%;">
    <div class="row well">
        <h1>Mantenedor de Sistema Workflow para TI (WFTI)</h1>
        <br>
        <div class="btn-group" role="group" aria-label="...">
            <a class="btn btn-default" href="${pageContext.request.contextPath}/secure/usuarios.htm">Usuarios</a>
            <a class="btn btn-default" href="${pageContext.request.contextPath}/secure/grupos.htm">Grupos</a>
            <a class="btn btn-default" href="${pageContext.request.contextPath}/secure/actividades.htm">Actividades</a>
            <a class="btn btn-default" href="${pageContext.request.contextPath}/secure/proyectos.htm">Proyectos</a>
        </div>
    </div>