<%--
  Created by IntelliJ IDEA.
  User: ulayera
  Date: 11/11/14
  Time: 3:36 AM
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <jsp:include page="common/scripts-view-section.jsp"/>
    <script src="http://crypto-js.googlecode.com/svn/tags/3.1.2/build/rollups/sha1.js"></script>
    <title>WFTI :: Mantenedor :: Proyectos</title>
</head>
<body>
<jsp:include page="common/menu-view.jsp"/>

<div class="row well">
    <div class="col-lg-6">
        <div class="row top-buffer">
            <div class="col-lg-6">
                <label for="id">ID del proyecto</label>
            </div>
            <div class="col-lg-6">
                <input id="id" type="text" style="width: 100%" disabled/>
            </div>
        </div>
        <div class="row top-buffer">
            <div class="col-lg-6">
                <label for="nombre">Nombre del proyecto</label>
            </div>
            <div class="col-lg-6">
                <input id="nombre" type="text" style="width: 100%"/>
            </div>
        </div>
        <div class="row top-buffer">
            <div class="col-lg-6">
            </div>
            <div class="col-lg-6" id="divBotones">
                <button id="btnCrear">Crear</button>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <table class="table">
            <thead>
            <tr>
                <th>Actividades</th>
                <th>Orden</th>
            </tr>
            </thead>
            <tbody id="listaProyectoActividad">
            <!--
            <tr>
                <td>Actividad ejemplo 1</td>
                <td><input type="text" maxlength="3" style="width: 35px"></td>
            </tr>
            <tr>
                <td>Actividad ejemplo 2</td>
                <td><input type="text" maxlength="3" style="width: 35px"></td>
            </tr>
            -->
            </tbody>
        </table>
    </div>
</div>

<div class="row well">
    <div class="col-lg-12">
        <table class="table">
            <thead>
            <tr>
                <th>ID del Proyecto</th>
                <th>Nombre del Proyecto</th>
                <th>Eliminar</th>
                <th>Editar</th>
            </tr>
            </thead>
            <tbody id="listaProyectos">

            </tbody>
        </table>
    </div>
</div>

</body>
<script src="/mantenedor/js/view-js/proyectos-view.js" type="application/javascript"></script>
<jsp:include page="common/footer-includes-view-section.jsp"/>
</html>