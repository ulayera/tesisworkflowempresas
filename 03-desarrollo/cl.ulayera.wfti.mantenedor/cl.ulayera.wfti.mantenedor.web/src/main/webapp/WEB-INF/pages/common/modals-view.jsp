<div id="areamensajes"></div>

<div class="modal fade" id="error-dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-danger">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><p id="err-title">&nbsp;</p></h4>
            </div>
            <div class="modal-body">
                <p id="err-msg">&nbsp;</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="confirmacion-dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-danger">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><p class="texto1" id="confirmacion-title">Atencion</p></h4>
            </div>
            <div class="modal-body">
                <p id="confirmacion-msg">&nbsp;</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" id="btnConfirmarCancelar">Cancelar</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal" id="btnConfirmarAceptar">Aceptar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="cargando-dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body" style="text-align:center">
                Cargando...<!--img src="img/Cargando.gif"/-->
            </div>
        </div>
    </div>
</div>

