<%--
  Created by IntelliJ IDEA.
  User: ulayera
  Date: 11/11/14
  Time: 3:36 AM
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <jsp:include page="common/scripts-view-section.jsp"/>
    <script src="http://crypto-js.googlecode.com/svn/tags/3.1.2/build/rollups/sha1.js"></script>
    <title>WFTI :: Mantenedor :: Grupos</title>
</head>
<body>
<jsp:include page="common/menu-view.jsp"/>

<div class="row well">
    <div class="col-lg-6">
        <div class="row top-buffer">
            <div class="col-lg-6">
                <label for="id">ID de Grupo </label>
            </div>
            <div class="col-lg-6">
                <input id="id" type="text" style="width: 100%" disabled/>
            </div>
        </div>
        <div class="row top-buffer">
            <div class="col-lg-6">
                <label for="nombre">Nombre de grupo</label>
            </div>
            <div class="col-lg-6">
                <input id="nombre" type="text" style="width: 100%"/>
            </div>
        </div>
        <div class="row top-buffer">
            <div class="col-lg-6">
            </div>
            <div class="col-lg-6" id="divBotones">
                <button id="btnCrear">Crear</button>
            </div>
        </div>
    </div>
</div>

<div class="row well">
    <div class="col-lg-12">
        <table class="table">
            <thead>
            <tr>
                <th>ID de Grupo</th>
                <th>Nombre del Grupo</th>
                <th>Eliminar</th>
                <th>Editar</th>
            </tr>
            </thead>
            <tbody id="listaGrupos">

            </tbody>
        </table>
    </div>
</div>
</body>
<script src="/mantenedor/js/view-js/grupos-view.js" type="application/javascript"></script>
<jsp:include page="common/footer-includes-view-section.jsp"/>
</html>