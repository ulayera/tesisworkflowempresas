<%--
  Created by IntelliJ IDEA.
  User: ulayera
  Date: 11/11/14
  Time: 3:36 AM
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <jsp:include page="common/scripts-view-section.jsp"/>
    <script src="http://crypto-js.googlecode.com/svn/tags/3.1.2/build/rollups/sha1.js"></script>
    <title>WFTI :: Mantenedor :: Usuarios</title>
</head>
<body>
<jsp:include page="common/menu-view.jsp"/>

<div class="row well">
    <div class="col-lg-6">
        <div class="row top-buffer">
            <div class="col-lg-6">
                <label for="id">ID de Usuario </label>
            </div>
            <div class="col-lg-6">
                <input id="id" type="text" style="width: 100%" disabled/>
            </div>
        </div>
        <div class="row top-buffer">
            <div class="col-lg-6">
                <label for="username">Username </label>
            </div>
            <div class="col-lg-6">
                <input id="username" type="text" style="width: 100%"/>
            </div>
        </div>
        <div class="row top-buffer">
            <div class="col-lg-6">
                <label for="nombre">Nombre </label>
            </div>
            <div class="col-lg-6">
                <input id="nombre" type="text" style="width: 100%"/>
            </div>
        </div>
        <div class="row top-buffer">
            <div class="col-lg-6">
                <label for="password">Contraseña </label>
            </div>
            <div class="col-lg-6">
                <input id="password" type="text" style="width: 100%"/>
            </div>
        </div>
        <div class="row top-buffer">
            <div class="col-lg-6">
                <label for="roles">Rol </label>
            </div>
            <div class="col-lg-6" id="roles">
                <div class="row top-buffer">
                    <div class="col-lg-4"><label for="rolUsuario">Usuario </label></div>
                    <div class="col-lg-1"><input id="rolUsuario" name="rol" type="radio" value="user" checked></div>
                    <div class="col-lg-5"><label for="rolAdmin">Administrador </label></div>
                    <div class="col-lg-1"><input id="rolAdmin"  name="rol" type="radio" value="admin"></div>
                </div>
            </div>
        </div>

        <div class="row top-buffer">
            <div class="col-lg-6">
            </div>
            <div class="col-lg-6" id="divBotones">
                <button id="btnCrear">Crear</button>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <table class="table">
            <thead>
            <tr>
                <th>Grupos</th>
                <th>Pertenece a</th>
            </tr>
            </thead>
            <tbody id="listaGruposUsuario">
            </tbody>
        </table>
    </div>
</div>

<div class="row well">
    <div class="col-lg-12">
        <table class="table">
            <thead>
            <tr>
                <th>ID de Usuario</th>
                <th>Username</th>
                <th>Nombre del Usuario</th>
                <th>Eliminar</th>
                <th>Editar</th>
            </tr>
            </thead>
            <tbody id="listaUsuarios">

            </tbody>
        </table>
    </div>
</div>

</body>
<script src="/mantenedor/js/view-js/usuarios-view.js" type="application/javascript"></script>
<jsp:include page="common/footer-includes-view-section.jsp"/>
</html>