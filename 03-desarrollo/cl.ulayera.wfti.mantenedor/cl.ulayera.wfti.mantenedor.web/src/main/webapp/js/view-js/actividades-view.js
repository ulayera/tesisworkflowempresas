var actividadesFunctions = {
    findAllActividades : function(){
        $("#listaActividades").html('');
        console.log("entrando a findAllActividades");
        $.ajax({
            type: "GET",
            url: "/mantenedor/api/actividades/",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).success(function(data, textStatus, jQxhr){
            actividadesFunctions.renderListaActividades(data.body);
        }).error(function(jqXhr, textStatus, errorThrown ){
            console.log('error');
            console.log(errorThrown);
            showListaErrores(jqXhr.mensajesError);
            showMensajesWarning(jqXhr.mensajesWarning);
        });
    },
    renderListaActividades : function(listaActividades) {
        console.log("entrando a renderListaActividades");
        if (listaActividades && listaActividades.length > 0) {
            $("#listaActividades").html('');
            for (var i = 0; i <listaActividades.length; i++) {
                var html = '<tr>'+
                    '<td>'+listaActividades[i].id+'</td>'+
                    '<td>'+listaActividades[i].nombre+'</td>'+
                    '<td width="100px"><button type="button" name="btnBorrar" class="btn btn-default btn-lg" onclick="actividadesFunctions.deleteActividad('+listaActividades[i].id+')"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button></td>'+
                    '<td width="100px"><button type="button" name="btnEditar" class="btn btn-default btn-lg" onclick="actividadesFunctions.editActividad('+listaActividades[i].id+')"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></button></td>'+
                    '</tr>';
                $("#listaActividades").html($("#listaActividades").html() + html);
            }
        }
    },
    findAllGrupos : function(){
        console.log("entrando a findAllGrupos");
        $.ajax({
            type: "GET",
            url: "/mantenedor/api/grupos/",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).success(function(data, textStatus, jQxhr){
            actividadesFunctions.renderListaGrupos(data.body);
        }).error(function(jqXhr, textStatus, errorThrown ){
            console.log('error');
            console.log(errorThrown);
            showListaErrores(jqXhr.mensajesError);
            showMensajesWarning(jqXhr.mensajesWarning);
        });
    },
    renderListaGrupos : function(listaGrupos) {
        console.log("entrando a renderListaGrupos");
        if (listaGrupos && listaGrupos.length > 0) {
            $("#listaGruposActividad").html('');
            for (var i = 0; i <listaGrupos.length; i++) {
                var html =  '<tr>'+
                                '<td>'+listaGrupos[i].nombre+'</td>'+
                                '<td><input name="grupos" value="'+listaGrupos[i].id+'" type="checkbox"></td>'+
                            '</tr>';

                $("#listaGruposActividad").html($("#listaGruposActividad").html() + html);
            }
        }
    },
    clearForm: function(){
        $("input[type='text']").val('');
        $("input[name='grupos']").attr('checked', false);
        $("#descripcion").val('');
    },
    deleteActividad: function(id) {
        console.log("entrando a deleteActividad");
        if(confirm("Se eliminará la actividad, desea continuar?")){
            var actividad = {};
            actividad.id = id;
            console.log(actividad);
            $.ajax({
                type: "DELETE",
                url: "/mantenedor/api/actividades/",
                data: JSON.stringify(actividad),
                headers: {
                    'Content-Type': 'application/json'
                }
            }).success(function(data, textStatus, jQxhr){
                actividadesFunctions.findAllActividades();
                actividadesFunctions.clearForm();
                console.log("ok");
            }).error(function(jqXhr, textStatus, errorThrown ){
                console.log('error');
                console.log(errorThrown);
                showListaErrores(jqXhr.mensajesError);
                showMensajesWarning(jqXhr.mensajesWarning);
            });
        }
    },
    editActividad: function(id) {
        console.log("entrando a editActividad");
        actividadesFunctions.clearForm();
        $.ajax({
            type: "GET",
            url: "/mantenedor/api/actividades/"+id+"/",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).success(function(data, textStatus, jQxhr){
            console.log(data.body);
            $("#btnCrear").html("Editar");
            $("#btnCrear").unbind("click");
            $("#btnCrear").bind("click",actividadesFunctions.doEditActividad);
            $("#btnCancelar").remove();
            $("#divBotones").append('<button id="btnCancelar">Cancelar</button>');
            $("#btnCancelar").bind("click",actividadesFunctions.cancelEdit);
            $("#id").val(data.body.id);
            $("#nombre").val(data.body.nombre);
            $("#descripcion").val(data.body.descripcion);
            var listaGrupos = data.body.idGrupos;
            for (var i = 0; i < listaGrupos.length; i++){
                $("input[name=grupos]").each(function(index, element){
                    console.log(element);
                    if (element.value == listaGrupos[i]){
                        element.checked = true;
                    }
                });
            }
        }).error(function(jqXhr, textStatus, errorThrown ){
            console.log('error');
            console.log(errorThrown);
            showListaErrores(jqXhr.mensajesError);
            showMensajesWarning(jqXhr.mensajesWarning);
        });
    },
    cancelEdit: function() {
        actividadesFunctions.clearForm();
        $("#btnCrear").html("Crear");
        $("#btnCrear").unbind("click");
        $("#btnCrear").bind("click",actividadesFunctions.createActividad);
        $("#btnCancelar").unbind("click");
        $("#btnCancelar").remove();
    },
    doEditActividad: function(id) {
        console.log("entrando a doEditActividad");
        if(confirm("Se modificará la actividad, desea continuar?")){
            var actividad = {};
            actividad.id = $("#id").val();
            actividad.nombre = $("#nombre").val();
            actividad.descripcion = $("#descripcion").val();
            var idGrupos = [];
            $("input[name=grupos]:checked").each(function(){
                idGrupos.push($(this).val());
            });
            actividad.idGrupos = idGrupos;
            $.ajax({
                type: "PUT",
                url: "/mantenedor/api/actividades/",
                data: JSON.stringify(actividad),
                headers: {
                    'Content-Type': 'application/json'
                }
            }).success(function(data, textStatus, jQxhr){
                actividadesFunctions.findAllActividades();
                actividadesFunctions.clearForm();
                $("#btnCrear").html("Crear");
                $("#btnCrear").unbind("click");
                $("#btnCrear").bind("click",actividadesFunctions.createActividad);
                console.log("ok");
            }).error(function(jqXhr, textStatus, errorThrown ){
                console.log('error');
                console.log(errorThrown);
                showListaErrores(jqXhr.mensajesError);
                showMensajesWarning(jqXhr.mensajesWarning);
            });
        }
    },
    createActividad: function(){
        var actividad = {};
        actividad.nombre = $("#nombre").val();
        actividad.descripcion = $("#descripcion").val();
        var idGrupos = [];
        $("input[name=grupos]:checked").each(function(){
            idGrupos.push($(this).val());
        });
        actividad.idGrupos = idGrupos;
        console.log(actividad);

        $.ajax({
            type: "POST",
            url: "/mantenedor/api/actividades/",
            data: JSON.stringify(actividad),
            headers: {
                'Content-Type': 'application/json'
            }
        }).success(function(data, textStatus, jQxhr){
            actividadesFunctions.findAllActividades();
            actividadesFunctions.clearForm();
            console.log("ok");
        }).error(function(jqXhr, textStatus, errorThrown ){
            console.log('error');
            console.log(errorThrown);
            showListaErrores(jqXhr.mensajesError);
            showMensajesWarning(jqXhr.mensajesWarning);
        });
    }
};
//bindings
actividadesFunctions.findAllActividades();
actividadesFunctions.findAllGrupos();
$("#btnCrear").bind("click",actividadesFunctions.createActividad);
