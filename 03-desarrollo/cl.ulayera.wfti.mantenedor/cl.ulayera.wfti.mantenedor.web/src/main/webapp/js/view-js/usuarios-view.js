var usuariosFunctions = {
    findAllUsuarios : function(){
        console.log("entrando a findAllUsuarios");
        $.ajax({
            type: "GET",
            url: "/mantenedor/api/usuarios/",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).success(function(data, textStatus, jQxhr){
            usuariosFunctions.renderListaUsuarios(data.body);
        }).error(function(jqXhr, textStatus, errorThrown ){
            console.log('error');
            console.log(errorThrown);
            showListaErrores(jqXhr.mensajesError);
            showMensajesWarning(jqXhr.mensajesWarning);
        });
    },
    renderListaUsuarios : function(listaUsuarios) {
        console.log("entrando a renderListaUsuarios");
        if (listaUsuarios && listaUsuarios.length > 0) {
            $("#listaUsuarios").html('');
            for (var i = 0; i <listaUsuarios.length; i++) {
                var html = '<tr>'+
                    '<td>'+listaUsuarios[i].id+'</td>'+
                    '<td>'+listaUsuarios[i].username+'</td>'+
                    '<td>'+listaUsuarios[i].nombre+'</td>'+
                    '<td width="100px"><button type="button" name="btnBorrar" class="btn btn-default btn-lg" onclick="usuariosFunctions.deleteUsuario('+listaUsuarios[i].id+')"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button></td>'+
                    '<td width="100px"><button type="button" name="btnEditar" class="btn btn-default btn-lg" onclick="usuariosFunctions.editUsuario('+listaUsuarios[i].id+')"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></button></td>'+
                    '</tr>';
                $("#listaUsuarios").html($("#listaUsuarios").html() + html);
            }
        }
    },
    findAllGrupos : function(){
        console.log("entrando a findAllGrupos");
        $.ajax({
            type: "GET",
            url: "/mantenedor/api/grupos/",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).success(function(data, textStatus, jQxhr){
            usuariosFunctions.renderListaGrupos(data.body);
        }).error(function(jqXhr, textStatus, errorThrown ){
            console.log('error');
            console.log(errorThrown);
            showListaErrores(jqXhr.mensajesError);
            showMensajesWarning(jqXhr.mensajesWarning);
        });
    },
    renderListaGrupos : function(listaGrupos) {
        console.log("entrando a renderListaGrupos");
        if (listaGrupos && listaGrupos.length > 0) {
            $("#listaGruposUsuario").html('');
            for (var i = 0; i <listaGrupos.length; i++) {
                var html =  '<tr>'+
                                '<td>'+listaGrupos[i].nombre+'</td>'+
                                '<td><input name="grupos" value="'+listaGrupos[i].id+'" type="checkbox"></td>'+
                            '</tr>';

                $("#listaGruposUsuario").html($("#listaGruposUsuario").html() + html);
            }
        }
    },
    clearForm: function(){
        $("input[type='text']").val('');
        $("input[name='grupos']").attr('checked', false);
    },
    deleteUsuario: function(id) {
        console.log("entrando a deleteUsuario");
        if(confirm("Se eliminará el usuario, desea continuar?")){
            var usuario = {};
            usuario.id = id;
            console.log(usuario);
            $.ajax({
                type: "DELETE",
                url: "/mantenedor/api/usuarios/",
                data: JSON.stringify(usuario),
                headers: {
                    'Content-Type': 'application/json'
                }
            }).success(function(data, textStatus, jQxhr){
                usuariosFunctions.findAllUsuarios();
                usuariosFunctions.clearForm();
                console.log("ok");
            }).error(function(jqXhr, textStatus, errorThrown ){
                console.log('error');
                console.log(errorThrown);
                showListaErrores(jqXhr.mensajesError);
                showMensajesWarning(jqXhr.mensajesWarning);
            });
        }
    },
    editUsuario: function(id) {
        console.log("entrando a editUsuario");
        usuariosFunctions.clearForm();
        $.ajax({
            type: "GET",
            url: "/mantenedor/api/usuarios/"+id+"/",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).success(function(data, textStatus, jQxhr){
            console.log(data.body);
            $("#btnCrear").html("Editar");
            $("#btnCrear").unbind("click");
            $("#btnCrear").bind("click",usuariosFunctions.doEditUsuario);
            $("#btnCancelar").remove();
            $("#divBotones").append('<button id="btnCancelar">Cancelar</button>');
            $("#btnCancelar").bind("click",usuariosFunctions.cancelEdit);
            $("#id").val(data.body.id);
            $("#username").val(data.body.username);
            $("#nombre").val(data.body.nombre);
            $("#password").val(data.body.password);
            var listaGrupos = data.body.idGrupos;
            for (var i = 0; i < listaGrupos.length; i++){
                $("input[name=grupos]").each(function(index, element){
                    if (element.value == listaGrupos[i]){
                        element.checked = true;
                    }
                });
            }
        }).error(function(jqXhr, textStatus, errorThrown ){
            console.log('error');
            console.log(errorThrown);
            showListaErrores(jqXhr.mensajesError);
            showMensajesWarning(jqXhr.mensajesWarning);
        });
    },
    cancelEdit: function() {
        usuariosFunctions.clearForm();
        $("#btnCrear").html("Crear");
        $("#btnCrear").unbind("click");
        $("#btnCrear").bind("click",usuariosFunctions.createUsuario);
        $("#btnCancelar").unbind("click");
        $("#btnCancelar").remove();
    },
    doEditUsuario: function(id) {
        console.log("entrando a doEditUsuario");
        if(confirm("Se modificará el usuario, desea continuar?")){
            var usuario = {};
            usuario.id = $("#id").val();
            usuario.username = $("#username").val();
            usuario.nombre = $("#nombre").val();
            usuario.password = $("#password").val();
            usuario.rol = $("input[name='rol']:radio:checked").val();
            var idGrupos = [];
            $("input[name=grupos]:checked").each(function(){
                idGrupos.push($(this).val());
            });
            usuario.idGrupos = idGrupos;
            $.ajax({
                type: "PUT",
                url: "/mantenedor/api/usuarios/",
                data: JSON.stringify(usuario),
                headers: {
                    'Content-Type': 'application/json'
                }
            }).success(function(data, textStatus, jQxhr){
                usuariosFunctions.findAllUsuarios();
                usuariosFunctions.cancelEdit();
                console.log("ok");
            }).error(function(jqXhr, textStatus, errorThrown ){
                console.log('error');
                console.log(errorThrown);
                showListaErrores(jqXhr.mensajesError);
                showMensajesWarning(jqXhr.mensajesWarning);
            });
        }
    },
    createUsuario: function(){
        var usuario = {};
        usuario.username = $("#username").val();
        usuario.nombre = $("#nombre").val();
        usuario.password = $("#password").val();
        usuario.rol = $("input[name='rol']:radio:checked").val();
        var idGrupos = [];
        $("input[name=grupos]:checked").each(function(){
            idGrupos.push($(this).val());
        });
        usuario.idGrupos = idGrupos;
        console.log(usuario);

        $.ajax({
            type: "POST",
            url: "/mantenedor/api/usuarios/",
            data: JSON.stringify(usuario),
            headers: {
                'Content-Type': 'application/json'
            }
        }).success(function(data, textStatus, jQxhr){
            usuariosFunctions.findAllUsuarios();
            usuariosFunctions.clearForm();
            console.log("ok");
        }).error(function(jqXhr, textStatus, errorThrown ){
            console.log('error');
            console.log(errorThrown);
            showListaErrores(jqXhr.mensajesError);
            showMensajesWarning(jqXhr.mensajesWarning);
        });
    }
};
//bindings
usuariosFunctions.findAllUsuarios();
usuariosFunctions.findAllGrupos();
$("#btnCrear").bind("click",usuariosFunctions.createUsuario);

