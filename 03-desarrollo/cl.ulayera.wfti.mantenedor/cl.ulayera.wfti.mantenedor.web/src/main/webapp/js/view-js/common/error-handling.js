function showListaErrores(errs) {
    var buffer = [];
    var errores = errs;
    if (errores) {
        if (errores.length == 1) {
            buffer.push('<p class="alert alert-info">Se ha producido el siguiente error:</p>');
        } else {
            buffer.push('<p class="alert alert-info">Se han producido <span class="badge">' + errores.length + '</span> errores:</p>');
        }
        $.each(errores, function () {
            buffer.push('<p>&nbsp;&nbsp;&nbsp;-&nbsp;' + this + '</p>');
        });
        showErrDlg(buffer.join(''));
    }
}

function showErrDlg(errMsg) {
    $('#err-msg').html(errMsg);
    $('#error-dialog').modal();
}

function showMensajesInfo(msjs){
    var buffer = [];
    var errores = msjs;
    if (errores) {
        $.each(errores, function () {
            buffer.push('<p class="alert alert-info">&nbsp;&nbsp;&nbsp;-&nbsp;' + this + '</p>');
        });
        $('#areamensajes').html($('#areamensajes').html() + buffer.join(''));
    }
}

function showMensajesWarning(msjs){
    var buffer = [];
    var errores = msjs;
    if (errores) {
        $.each(errores, function () {
            buffer.push('<p class="alert alert-warning">&nbsp;&nbsp;&nbsp;-&nbsp;' + this + '</p>');
        });
        $('#areamensajes').html($('#areamensajes').html() + buffer.join(''));
    }
}