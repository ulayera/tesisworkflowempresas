var proyectosFunctions = {
    findAllProyectos : function(){
        console.log("entrando a findAllProyectos");
        $.ajax({
            type: "GET",
            url: "/mantenedor/api/proyectos/",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).success(function(data, textStatus, jQxhr){
            proyectosFunctions.renderListaProyectos(data.body);
        }).error(function(jqXhr, textStatus, errorThrown ){
            console.log('error');
            console.log(errorThrown);
            showListaErrores(jqXhr.mensajesError);
            showMensajesWarning(jqXhr.mensajesWarning);
        });
    },
    renderListaProyectos : function(listaProyectos) {
        console.log("entrando a renderListaProyectos");
        if (listaProyectos && listaProyectos.length > 0) {
            $("#listaProyectos").html('');
            for (var i = 0; i <listaProyectos.length; i++) {
                var html = '<tr id="tr_'+listaProyectos[i].id+'">'+
                    '<td>'+listaProyectos[i].id+'</td>'+
                    '<td>'+listaProyectos[i].nombre+'</td>'+
                    '<td width="100px"><button type="button" name="btnBorrar" class="btn btn-default btn-lg" onclick="proyectosFunctions.deleteProyecto('+listaProyectos[i].id+')"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button></td>'+
                    '<td width="100px"><button type="button" name="btnEditar" class="btn btn-default btn-lg" onclick="proyectosFunctions.editProyecto('+listaProyectos[i].id+')"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></button></td>'+
                    '</tr>';
                $("#listaProyectos").html($("#listaProyectos").html() + html);
            }
        }
    },findAllActividades : function(){
        console.log("entrando a findAllGrupos");
        $.ajax({
            type: "GET",
            url: "/mantenedor/api/actividades/",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).success(function(data, textStatus, jQxhr){
            proyectosFunctions.renderListaActividades(data.body);
        }).error(function(jqXhr, textStatus, errorThrown ){
            console.log('error');
            console.log(errorThrown);
            showListaErrores(jqXhr.mensajesError);
            showMensajesWarning(jqXhr.mensajesWarning);
        });
    },
    cancelEdit: function() {
        proyectosFunctions.clearForm();
        $("#btnCrear").html("Crear");
        $("#btnCrear").unbind("click");
        $("#btnCrear").bind("click",proyectosFunctions.createProyecto());
        $("#btnCancelar").unbind("click");
        $("#btnCancelar").remove();
    },
    renderListaActividades : function(listaActividades) {
        console.log("entrando a renderListaGrupos");
        if (listaActividades && listaActividades.length > 0) {
            $("#listaProyectoActividad").html('');
            for (var i = 0; i <listaActividades.length; i++) {
                var html =  '<tr>'+
                                '<td>'+listaActividades[i].nombre+'</td>'+
                                '<td><input type="text" id="'+ listaActividades[i].id +'" maxlength="3" style="width: 35px"></td>'+
                            '</tr>';
                $("#listaProyectoActividad").html($("#listaProyectoActividad").html() + html);
            }
        }
    },
    clearForm: function(){
        $("input[type='text']").val('');
    },
    deleteProyecto: function(id) {
        console.log("entrando a deleteUsuario");
        if(confirm("Se eliminará el proyecto, desea continuar?")){
            var proyecto = {};
            proyecto.id = id;
            console.log(proyecto);
            $.ajax({
                type: "DELETE",
                url: "/mantenedor/api/proyectos/",
                data: JSON.stringify(proyecto),
                headers: {
                    'Content-Type': 'application/json'
                }
            }).success(function(data, textStatus, jQxhr){
                $('#tr_'+id).remove();
                proyectosFunctions.clearForm();
                console.log("ok");
            }).error(function(jqXhr, textStatus, errorThrown ){
                console.log('error');
                console.log(errorThrown);
                showListaErrores(jqXhr.mensajesError);
                showMensajesWarning(jqXhr.mensajesWarning);
            });
        }
    },
    editProyecto: function(id) {
        console.log("entrando a editProyecto");
        proyectosFunctions.clearForm();
        $.ajax({
            type: "GET",
            url: "/mantenedor/api/proyectos/"+id+"/",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).success(function(data, textStatus, jQxhr){
            console.log(data.body);
            $("#btnCrear").html("Editar");
            $("#nombre").val(data.body.nombre);
            $("#id").val(data.body.id);
            var proyecto = data.body;
            if(proyecto.actividades.length > 0) {
                for(var i = 0; i < proyecto.actividades.length; i++) {
                    $('#'+proyecto.actividades[i]).val(proyecto.ordenes[i]);
                }
            }
            $("#btnCrear").unbind("click");
            $("#btnCrear").bind("click",proyectosFunctions.doEditProyecto);
            $("#btnCancelar").unbind("click");
            $("#btnCancelar").remove();
            $("#divBotones").append('<button id="btnCancelar">Cancelar</button>');
            $("#btnCancelar").bind("click",proyectosFunctions.cancelEdit);

        }).error(function(jqXhr, textStatus, errorThrown ){
            console.log('error');
            console.log(errorThrown);
            showListaErrores(jqXhr.mensajesError);
            showMensajesWarning(jqXhr.mensajesWarning);
        });
    },
    doEditProyecto: function(id) {
        console.log("entrando a doEditUsuario");
        var proyecto = {};
        proyecto.nombre = $("#nombre").val();
        proyecto.id = $('#id').val();
        var ordenes = [];
        var idActividades = [];
        $("#listaProyectoActividad").find("input[type=text]").each(function(){
            var val = $.trim($(this).val());
            var id = this.id;
            if(val != null && val.length > 0) {
                idActividades.push(id);
                ordenes.push(val);
            }

        });
        proyecto.actividades = idActividades;
        proyecto.ordenes = ordenes;
        console.log(proyecto);

        $.ajax({
            type: "PUT",
            url: "/mantenedor/api/proyectos/",
            data: JSON.stringify(proyecto),
            headers: {
                'Content-Type': 'application/json'
            }
        }).success(function(data, textStatus, jQxhr){
            proyectosFunctions.findAllProyectos();
            proyectosFunctions.clearForm();
            $("#btnCrear").unbind("click");
            $("#btnCrear").bind("click",proyectosFunctions.createProyecto);
            $("#btnCrear").html("Crear");
            console.log("ok");
        }).error(function(jqXhr, textStatus, errorThrown ){
            console.log('error');
            console.log(errorThrown);
            showListaErrores(jqXhr.mensajesError);
            showMensajesWarning(jqXhr.mensajesWarning);
        });
    },
    createProyecto: function(){
        var proyecto = {};
        proyecto.nombre = $("#nombre").val();
        var ordenes = [];
        var idActividades = [];
        $("#listaProyectoActividad").find("input[type=text]").each(function(){
            var val = $.trim($(this).val());
            var id = this.id;
            if(val != null && val.length > 0) {
                idActividades.push(id);
                ordenes.push(val);
            }

        });
        proyecto.actividades = idActividades;
        proyecto.ordenes = ordenes;
        console.log(proyecto);

        $.ajax({
            type: "POST",
            url: "/mantenedor/api/proyectos/",
            data: JSON.stringify(proyecto),
            headers: {
                'Content-Type': 'application/json'
            }
        }).success(function(data, textStatus, jQxhr){
            proyectosFunctions.findAllProyectos();
            proyectosFunctions.clearForm();
            console.log("ok");
        }).error(function(jqXhr, textStatus, errorThrown ){
            console.log('error');
            console.log(errorThrown);
            showListaErrores(jqXhr.mensajesError);
            showMensajesWarning(jqXhr.mensajesWarning);
        });
    }
};
//bindings
proyectosFunctions.findAllProyectos();
proyectosFunctions.findAllActividades();
$("#btnCrear").bind("click",proyectosFunctions.createProyecto);
