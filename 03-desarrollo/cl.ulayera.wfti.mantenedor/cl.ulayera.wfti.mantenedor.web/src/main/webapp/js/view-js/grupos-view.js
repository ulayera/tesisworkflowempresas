var gruposFunctions = {
    findAllGrupos : function(){
        console.log("entrando a findAllGrupos");
        $.ajax({
            type: "GET",
            url: "/mantenedor/api/grupos/",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).success(function(data, textStatus, jQxhr){
            gruposFunctions.renderListaGrupos(data.body);
        }).error(function(jqXhr, textStatus, errorThrown ){
            console.log('error');
            console.log(errorThrown);
            showListaErrores(jqXhr.mensajesError);
            showMensajesWarning(jqXhr.mensajesWarning);
        });
    },
    renderListaGrupos : function(listaGrupos) {
        console.log("entrando a renderListaGrupos");
        if (listaGrupos && listaGrupos.length > 0) {
            $("#listaGrupos").html('');
            for (var i = 0; i <listaGrupos.length; i++) {
                var html = '<tr>'+
                    '<td>'+listaGrupos[i].id+'</td>'+
                    '<td>'+listaGrupos[i].nombre+'</td>'+
                    '<td width="100px"><button type="button" name="btnBorrar" class="btn btn-default btn-lg" onclick="gruposFunctions.deleteGrupo('+listaGrupos[i].id+')"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button></td>'+
                    '<td width="100px"><button type="button" name="btnEditar" class="btn btn-default btn-lg" onclick="gruposFunctions.editGrupo('+listaGrupos[i].id+')"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></button></td>'+
                    '</tr>';
                $("#listaGrupos").html($("#listaGrupos").html() + html);
            }
        }
    },
    deleteGrupo: function(id){
        console.log("entrando a deleteGrupo");
        if(confirm("Se eliminará el grupo, desea continuar?")){
            var grupo = {};
            grupo.id = id;
            console.log(grupo);
            $.ajax({
                type: "DELETE",
                url: "/mantenedor/api/grupos/",
                data: JSON.stringify(grupo),
                headers: {
                    'Content-Type': 'application/json'
                }
            }).success(function(data, textStatus, jQxhr){
                gruposFunctions.findAllGrupos();
                gruposFunctions.clearForm();
                console.log("ok");
            }).error(function(jqXhr, textStatus, errorThrown ){
                console.log('error');
                console.log(errorThrown);
                showListaErrores(jqXhr.mensajesError);
                showMensajesWarning(jqXhr.mensajesWarning);
            });
        }
    },
    editGrupo: function(id){
        console.log("entrando a editGrupo");
        gruposFunctions.clearForm();
        $.ajax({
            type: "GET",
            url: "/mantenedor/api/grupos/"+id+"/",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).success(function(data, textStatus, jQxhr){
            console.log(data.body);
            $("#btnCrear").html("Editar");
            $("#btnCrear").unbind("click");
            $("#btnCrear").bind("click",gruposFunctions.doEditgrupo);
            $("#btnCancelar").unbind("click");
            $("#btnCancelar").remove();
            $("#divBotones").append('<button id="btnCancelar">Cancelar</button>');
            $("#btnCancelar").bind("click",gruposFunctions.cancelEdit);
            $("#id").val(data.body.id);
            $("#nombre").val(data.body.nombre);
        }).error(function(jqXhr, textStatus, errorThrown ){
            console.log('error');
            console.log(errorThrown);
            showListaErrores(jqXhr.mensajesError);
            showMensajesWarning(jqXhr.mensajesWarning);
        });
    },
    cancelEdit: function() {
        gruposFunctions.clearForm();
        $("#btnCrear").html("Crear");
        $("#btnCrear").unbind("click");
        $("#btnCrear").bind("click",gruposFunctions.createGrupo);
        $("#btnCancelar").unbind("click");
        $("#btnCancelar").remove();
    },
    doEditgrupo: function(){
        console.log("entrando a doEditgrupo");
        if(confirm("Se modificará el grupo, desea continuar?")){
            var grupo = {};
            grupo.id =     $("#id").val();
            grupo.nombre = $("#nombre").val();
            $.ajax({
                type: "PUT",
                url: "/mantenedor/api/grupos/",
                data: JSON.stringify(grupo),
                headers: {
                    'Content-Type': 'application/json'
                }
            }).success(function(data, textStatus, jQxhr){
                gruposFunctions.findAllGrupos();
                gruposFunctions.cancelEdit();
                console.log("ok");
            }).error(function(jqXhr, textStatus, errorThrown ){
                gruposFunctions.cancelEdit();
                console.log('error');
                console.log(errorThrown);
                showListaErrores(jqXhr.mensajesError);
                showMensajesWarning(jqXhr.mensajesWarning);
            });
        }
    },
    createGrupo: function(){
        var grupo = {};
        grupo.nombre = $("#nombre").val();
        console.log(grupo);

        $.ajax({
            type: "POST",
            url: "/mantenedor/api/grupos/",
            data: JSON.stringify(grupo),
            headers: {
                'Content-Type': 'application/json'
            }
        }).success(function(data, textStatus, jQxhr){
            gruposFunctions.findAllGrupos();
            gruposFunctions.clearForm();
            console.log("ok");
        }).error(function(jqXhr, textStatus, errorThrown ){
            console.log('error');
            console.log(errorThrown);
            showListaErrores(jqXhr.mensajesError);
            showMensajesWarning(jqXhr.mensajesWarning);
        });
    },
    clearForm: function(){
        $("input[type='text']").val('');
    }
};

gruposFunctions.findAllGrupos();
$("#btnCrear").bind("click",gruposFunctions.createGrupo);