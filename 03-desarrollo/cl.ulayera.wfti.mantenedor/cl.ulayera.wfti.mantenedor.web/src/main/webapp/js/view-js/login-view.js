var loginFunctions = {
    intentoLogin : function(){
        var usuario = {};
        usuario.username = $("#username").val();
        usuario.password = CryptoJS.SHA1($("#password").val()).toString();
        console.log(usuario);
        $.ajax({
            type: "POST",
            url: "login/intento.htm",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            data: JSON.stringify(usuario)
        }).success(function(data, textStatus, jQxhr){
            console.log(data);
            showMensajesInfo(data.mensajesInfo);
            var urldestino = $("#url-destino").val();
            console.log("ir a: " + urldestino);
            if (urldestino && urldestino != '') {
                window.location.href = urldestino;
            } else {
                window.location.href = 'secure/';
            }
        }).error(function(jqXhr, textStatus, errorThrown ){
            console.log('error');
            console.log(errorThrown);
            showListaErrores(jqXhr.mensajesError);
            showMensajesWarning(jqXhr.mensajesWarning);
        });
    }
};

$("#btnLogin").bind("click", loginFunctions.intentoLogin);