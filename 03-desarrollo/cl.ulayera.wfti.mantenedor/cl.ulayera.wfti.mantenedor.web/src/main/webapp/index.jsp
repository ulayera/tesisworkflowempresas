<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Test Page</title>
    </head>
    <body>
        <h1>Mantenedor Web Root</h1>
        <ul>
            <li><a href="login.htm">Login</a></li>
            <li><a href="secure.htm">Main</a></li>
            <li><a href="secure/usuarios.htm">Usuarios</a></li>
            <li><a href="secure/grupos.htm">Grupos</a></li>
            <li><a href="secure/actividades.htm">Actividades</a></li>
            <li><a href="secure/proyectos.htm">Proyectos</a></li>
        </ul>
    </body>
</html>
