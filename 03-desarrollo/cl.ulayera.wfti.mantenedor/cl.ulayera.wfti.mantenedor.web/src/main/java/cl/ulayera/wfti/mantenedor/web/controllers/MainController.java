package cl.ulayera.wfti.mantenedor.web.controllers;

import cl.ulayera.wfti.mantenedor.business.delegates.LoginServiceDelegator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by ulayera on 11/30/14.
 */
@Controller
public class MainController {
    private static final Logger LOGGER = Logger.getLogger(LoginController.class);

    @Autowired
    LoginServiceDelegator loginServiceDelegator;

    public MainController() {}

    @RequestMapping(value = "/secure")
    public ModelAndView ingreso(HttpServletRequest httpServletRequest) {
        LOGGER.debug("/secure");

        LOGGER.info("Ejecutando: " + new Object(){}.getClass().getName() + "." + new Object(){}.getClass().getEnclosingMethod().getName());
        return new ModelAndView("main-view");
    }
}
