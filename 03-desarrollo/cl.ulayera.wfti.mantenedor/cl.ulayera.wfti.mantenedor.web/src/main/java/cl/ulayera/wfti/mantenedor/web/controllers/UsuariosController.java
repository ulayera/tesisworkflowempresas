package cl.ulayera.wfti.mantenedor.web.controllers;

import cl.ulayera.wfti.mantenedor.api.exceptions.WftiException;
import cl.ulayera.wfti.mantenedor.api.vo.common.JsonResponse;
import cl.ulayera.wfti.mantenedor.api.vo.common.WftiRequest;
import cl.ulayera.wfti.mantenedor.api.vo.common.WftiResponse;
import cl.ulayera.wfti.mantenedor.api.vo.models.Usuario;
import cl.ulayera.wfti.mantenedor.business.services.UsuarioService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Creado por ulayera el 11/30/14.
 */
@Controller
public class UsuariosController {
    private static final Logger LOGGER = Logger.getLogger(LoginController.class);

    @Autowired
    UsuarioService usuarioService;

    public UsuariosController() {
    }

    @RequestMapping(value = "/secure/usuarios")
    public ModelAndView ingreso(HttpServletRequest httpServletRequest) {
        LOGGER.debug("/secure/usuarios");

        LOGGER.info("Ejecutando: " + new Object() {
        }.getClass().getName() + "." + new Object() {
        }.getClass().getEnclosingMethod().getName());
        return new ModelAndView("usuarios-view");
    }

    @RequestMapping(value = "/api/usuarios/", method = RequestMethod.GET)
    public
    @ResponseBody
    JsonResponse findAllUsuarios(HttpServletRequest request) {
        LOGGER.info("Ejecutando: " + new Object() {
        }.getClass().getName() + "." + new Object() {
        }.getClass().getEnclosingMethod().getName());

        WftiRequest wftiRequest = new WftiRequest();
        JsonResponse response = new JsonResponse();
        List<Usuario> usuarioList;
        try {
            WftiResponse wftiResponse = usuarioService.findAllUsuarios(wftiRequest);
            usuarioList = (List<Usuario>) wftiResponse.getResultado("usuarios");
            response.setBody(usuarioList);
            response.addMensajeInfo(wftiResponse.getMensajeSalida());
            response.setSuccessToTrue();
        } catch (WftiException e) {
            response.setSuccessToFalse();
            response.addMensajeError(e.getBusinessMessage());
        }

        return response;
    }

    @RequestMapping(value = "/api/usuarios/{id}/", method = RequestMethod.GET)
    public
    @ResponseBody
    JsonResponse findUsuarioById(HttpServletRequest request,
                                 @PathVariable Long id) {
        LOGGER.info("Ejecutando: " + new Object() {
        }.getClass().getName() + "." + new Object() {
        }.getClass().getEnclosingMethod().getName());

        WftiRequest wftiRequest = new WftiRequest();
        JsonResponse response = new JsonResponse();
        Usuario usuario;
        wftiRequest.addParam("id", id);
        try {
            WftiResponse wftiResponse = usuarioService.findUsuarioById(wftiRequest);
            usuario = (Usuario) wftiResponse.getResultado("usuario");
            response.setBody(usuario);
            response.addMensajeInfo(wftiResponse.getMensajeSalida());
            response.setSuccessToTrue();
        } catch (WftiException e) {
            response.setSuccessToFalse();
            response.addMensajeError(e.getBusinessMessage());
        }

        return response;
    }

    @RequestMapping(value = "/api/usuarios/", method = RequestMethod.POST)
    public
    @ResponseBody
    JsonResponse createUsuario(HttpServletRequest request,
                               @RequestBody Usuario usuario) {
        LOGGER.info("Ejecutando: " + new Object() {
        }.getClass().getName() + "." + new Object() {
        }.getClass().getEnclosingMethod().getName());

        WftiRequest wftiRequest = new WftiRequest();
        JsonResponse response = new JsonResponse();
        wftiRequest.addParam("usuario", usuario);
        try {
            WftiResponse wftiResponse = usuarioService.createUsuario(wftiRequest);
            usuario = (Usuario) wftiResponse.getResultado("usuario");
            response.setBody(usuario);
            response.addMensajeInfo(wftiResponse.getMensajeSalida());
            response.setSuccessToTrue();
        } catch (WftiException e) {
            response.setSuccessToFalse();
            response.addMensajeError(e.getBusinessMessage());
        }
        return response;
    }

    @RequestMapping(value = "/api/usuarios/", method = RequestMethod.DELETE)
    public
    @ResponseBody
    JsonResponse deleteUsuario(HttpServletRequest request,
                               @RequestBody Usuario usuario) {
        LOGGER.info("Ejecutando: " + new Object() {
        }.getClass().getName() + "." + new Object() {
        }.getClass().getEnclosingMethod().getName());

        WftiRequest wftiRequest = new WftiRequest();
        JsonResponse response = new JsonResponse();
        wftiRequest.addParam("usuario", usuario);
        try {
            WftiResponse wftiResponse = usuarioService.deleteUsuario(wftiRequest);
            usuario = (Usuario) wftiResponse.getResultado("usuario");
            response.setBody(usuario);
            response.addMensajeInfo(wftiResponse.getMensajeSalida());
            response.setSuccessToTrue();
        } catch (WftiException e) {
            response.setSuccessToFalse();
            response.addMensajeError(e.getBusinessMessage());
        }
        return response;
    }

    @RequestMapping(value = "/api/usuarios/", method = RequestMethod.PUT)
    public
    @ResponseBody
    JsonResponse editUsuario(HttpServletRequest request,
                               @RequestBody Usuario usuario) {
        LOGGER.info("Ejecutando: " + new Object() {
        }.getClass().getName() + "." + new Object() {
        }.getClass().getEnclosingMethod().getName());

        WftiRequest wftiRequest = new WftiRequest();
        JsonResponse response = new JsonResponse();
        wftiRequest.addParam("usuario", usuario);
        try {
            WftiResponse wftiResponse = usuarioService.editUsuario(wftiRequest);
            response.addMensajeInfo(wftiResponse.getMensajeSalida());
            response.setSuccessToTrue();
        } catch (WftiException e) {
            response.setSuccessToFalse();
            response.addMensajeError(e.getBusinessMessage());
        }
        return response;
    }
}
