package cl.ulayera.wfti.mantenedor.web.controllers;

import cl.ulayera.wfti.mantenedor.api.exceptions.WftiException;
import cl.ulayera.wfti.mantenedor.api.vo.common.JsonResponse;
import cl.ulayera.wfti.mantenedor.api.vo.common.WftiRequest;
import cl.ulayera.wfti.mantenedor.api.vo.common.WftiResponse;
import cl.ulayera.wfti.mantenedor.api.vo.models.Proyecto;
import cl.ulayera.wfti.mantenedor.business.services.ProyectoService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Creado por ulayera el 11/30/14.
 */
@Controller
public class ProyectosController {
    private static final Logger LOGGER = Logger.getLogger(LoginController.class);

    @Autowired
    ProyectoService proyectoService;

    public ProyectosController() {}

    @RequestMapping(value = "/secure/proyectos")
    public ModelAndView ingreso(HttpServletRequest httpServletRequest) {
        LOGGER.debug("/secure/proyectos");

        LOGGER.info("Ejecutando: " + new Object(){}.getClass().getName() + "." + new Object(){}.getClass().getEnclosingMethod().getName());
        return new ModelAndView("proyectos-view");
    }

    @RequestMapping(value = "/api/proyectos/", method = RequestMethod.GET)
    public
    @ResponseBody
    JsonResponse findAllProyectos(HttpServletRequest request) {
        JsonResponse response = new JsonResponse();
        List<Proyecto> proyectoList;

        WftiRequest wftiRequest = new WftiRequest();
        try {
            WftiResponse wftiResponse = proyectoService.findAllProyectos(wftiRequest);
            proyectoList = (List<Proyecto>) wftiResponse.getResultado("proyectos");
            response.setBody(proyectoList);
            response.addMensajeInfo(wftiResponse.getMensajeSalida());
            response.setSuccessToTrue();
        } catch (WftiException e) {
            response.setSuccessToFalse();
            response.addMensajeError(e.getBusinessMessage());
        }

        return response;
    }

    @RequestMapping(value = "/api/proyectos/{id}/", method = RequestMethod.GET)
    public
    @ResponseBody
    JsonResponse findProyectoById(HttpServletRequest request,
                               @PathVariable Long id) {
        JsonResponse response = new JsonResponse();
        Proyecto proyecto;
        WftiRequest wftiRequest = new WftiRequest();
        wftiRequest.addParam("id", id);
        try {
            WftiResponse wftiResponse = proyectoService.findProyectoById(wftiRequest);
            proyecto = (Proyecto) wftiResponse.getResultado("proyecto");
            response.setBody(proyecto);
            response.addMensajeInfo(wftiResponse.getMensajeSalida());
            response.setSuccessToTrue();
        } catch (WftiException e) {
            response.setSuccessToFalse();
            response.addMensajeError(e.getBusinessMessage());
        }

        return response;
    }

    @RequestMapping(value = "/api/proyectos/", method = RequestMethod.POST)
    public
    @ResponseBody
    JsonResponse createProyecto(HttpServletRequest request,
                             @RequestBody Proyecto proyecto) {
        LOGGER.info("Ejecutando: " + new Object() {
        }.getClass().getName() + "." + new Object() {
        }.getClass().getEnclosingMethod().getName());

        WftiRequest wftiRequest = new WftiRequest();
        JsonResponse response = new JsonResponse();
        wftiRequest.addParam("proyecto", proyecto);
        try {
            WftiResponse wftiResponse = proyectoService.createProyecto(wftiRequest);
            proyecto = (Proyecto) wftiResponse.getResultado("proyecto");
            response.setBody(proyecto);
            response.addMensajeInfo(wftiResponse.getMensajeSalida());
            response.setSuccessToTrue();
        } catch (WftiException e) {
            response.setSuccessToFalse();
            response.addMensajeError(e.getBusinessMessage());
        }
        return response;
    }

    @RequestMapping(value = "/api/proyectos/", method = RequestMethod.DELETE)
    public
    @ResponseBody
    JsonResponse deleteProyecto(HttpServletRequest request,
                             @RequestBody Proyecto proyecto) {
        LOGGER.info("Ejecutando: " + new Object() {
        }.getClass().getName() + "." + new Object() {
        }.getClass().getEnclosingMethod().getName());

        WftiRequest wftiRequest = new WftiRequest();
        JsonResponse response = new JsonResponse();
        wftiRequest.addParam("proyecto", proyecto);
        try {
            WftiResponse wftiResponse = proyectoService.deleteProyecto(wftiRequest);
            proyecto = (Proyecto) wftiResponse.getResultado("proyecto");
            response.setBody(proyecto);
            response.addMensajeInfo(wftiResponse.getMensajeSalida());
            response.setSuccessToTrue();
        } catch (WftiException e) {
            response.setSuccessToFalse();
            response.addMensajeError(e.getBusinessMessage());
        }
        return response;
    }

    @RequestMapping(value = "/api/proyectos/", method = RequestMethod.PUT)
    public
    @ResponseBody
    JsonResponse editProyecto(HttpServletRequest request,
                             @RequestBody Proyecto proyecto) {
        LOGGER.info("Ejecutando: " + new Object() {
        }.getClass().getName() + "." + new Object() {
        }.getClass().getEnclosingMethod().getName());

        WftiRequest wftiRequest = new WftiRequest();
        JsonResponse response = new JsonResponse();
        wftiRequest.addParam("proyecto", proyecto);
        try {
            WftiResponse wftiResponse = proyectoService.updateProyecto(wftiRequest);
            response.addMensajeInfo(wftiResponse.getMensajeSalida());
            response.setSuccessToTrue();
        } catch (WftiException e) {
            response.setSuccessToFalse();
            response.addMensajeError(e.getBusinessMessage());
        }
        return response;
    }
}
