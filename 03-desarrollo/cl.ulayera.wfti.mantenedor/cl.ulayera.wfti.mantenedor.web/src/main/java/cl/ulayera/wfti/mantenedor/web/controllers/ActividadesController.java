package cl.ulayera.wfti.mantenedor.web.controllers;

import cl.ulayera.wfti.mantenedor.api.exceptions.WftiException;
import cl.ulayera.wfti.mantenedor.api.vo.common.JsonResponse;
import cl.ulayera.wfti.mantenedor.api.vo.common.WftiRequest;
import cl.ulayera.wfti.mantenedor.api.vo.common.WftiResponse;
import cl.ulayera.wfti.mantenedor.api.vo.models.Actividad;
import cl.ulayera.wfti.mantenedor.business.services.ActividadService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by ulayera on 11/30/14.
 */
@Controller
public class ActividadesController {
    private static final Logger LOGGER = Logger.getLogger(LoginController.class);

    @Autowired
    ActividadService actividadService;

    public ActividadesController() {}

    @RequestMapping(value = "/secure/actividades")
    public ModelAndView ingreso(HttpServletRequest httpServletRequest) {
        LOGGER.debug("/secure/actividades");

        LOGGER.info("Ejecutando: " + new Object(){}.getClass().getName() + "." + new Object(){}.getClass().getEnclosingMethod().getName());
        return new ModelAndView("actividades-view");
    }

    @RequestMapping(value = "/api/actividades/", method = RequestMethod.GET)
    public
    @ResponseBody
    JsonResponse findAllActividades(HttpServletRequest request) {
        JsonResponse response = new JsonResponse();
        List<Actividad> actividadList;

        WftiRequest wftiRequest = new WftiRequest();
        try {
            WftiResponse wftiResponse = actividadService.findAllActividades(wftiRequest);
            actividadList = (List<Actividad>) wftiResponse.getResultado("actividades");
            response.setBody(actividadList);
            response.addMensajeInfo(wftiResponse.getMensajeSalida());
            response.setSuccessToTrue();
        } catch (WftiException e) {
            response.setSuccessToFalse();
            response.addMensajeError(e.getBusinessMessage());
        }

        return response;
    }

    @RequestMapping(value = "/api/actividades/{id}/", method = RequestMethod.GET)
    public
    @ResponseBody
    JsonResponse findActividadById(HttpServletRequest request,
                                 @PathVariable Long id) {
        JsonResponse response = new JsonResponse();
        Actividad actividad;
        WftiRequest wftiRequest = new WftiRequest();
        wftiRequest.addParam("id",id);
        try {
            WftiResponse wftiResponse = actividadService.findActividadById(wftiRequest);
            actividad = (Actividad) wftiResponse.getResultado("actividad");
            response.setBody(actividad);
            response.addMensajeInfo(wftiResponse.getMensajeSalida());
            response.setSuccessToTrue();
        } catch (WftiException e) {
            response.setSuccessToFalse();
            response.addMensajeError(e.getBusinessMessage());
        }
        return response;
    }

    @RequestMapping(value = "/api/actividades/", method = RequestMethod.POST)
    public
    @ResponseBody
    JsonResponse createActividad(HttpServletRequest request,
                               @RequestBody Actividad actividad) {
        LOGGER.info("Ejecutando: " + new Object() {
        }.getClass().getName() + "." + new Object() {
        }.getClass().getEnclosingMethod().getName());

        WftiRequest wftiRequest = new WftiRequest();
        JsonResponse response = new JsonResponse();
        wftiRequest.addParam("actividad", actividad);
        try {
            WftiResponse wftiResponse = actividadService.createActividad(wftiRequest);
            actividad = (Actividad) wftiResponse.getResultado("actividad");
            response.setBody(actividad);
            response.addMensajeInfo(wftiResponse.getMensajeSalida());
            response.setSuccessToTrue();
        } catch (WftiException e) {
            response.setSuccessToFalse();
            response.addMensajeError(e.getBusinessMessage());
        }
        return response;
    }

    @RequestMapping(value = "/api/actividades/", method = RequestMethod.DELETE)
    public
    @ResponseBody
    JsonResponse deleteActividad(HttpServletRequest request,
                               @RequestBody Actividad actividad) {
        LOGGER.info("Ejecutando: " + new Object() {
        }.getClass().getName() + "." + new Object() {
        }.getClass().getEnclosingMethod().getName());

        WftiRequest wftiRequest = new WftiRequest();
        JsonResponse response = new JsonResponse();
        wftiRequest.addParam("actividad", actividad);
        try {
            WftiResponse wftiResponse = actividadService.deleteActividad(wftiRequest);
            actividad = (Actividad) wftiResponse.getResultado("actividad");
            response.setBody(actividad);
            response.addMensajeInfo(wftiResponse.getMensajeSalida());
            response.setSuccessToTrue();
        } catch (WftiException e) {
            response.setSuccessToFalse();
            response.addMensajeError(e.getBusinessMessage());
        }
        return response;
    }

    @RequestMapping(value = "/api/actividades/", method = RequestMethod.PUT)
    public
    @ResponseBody
    JsonResponse updateActividad(HttpServletRequest request,
                                 @RequestBody Actividad actividad) {
        LOGGER.info("Ejecutando: " + new Object() {
        }.getClass().getName() + "." + new Object() {
        }.getClass().getEnclosingMethod().getName());

        WftiRequest wftiRequest = new WftiRequest();
        JsonResponse response = new JsonResponse();
        wftiRequest.addParam("actividad", actividad);
        try {
            WftiResponse wftiResponse = actividadService.updateActividad(wftiRequest);
            actividad = (Actividad) wftiResponse.getResultado("actividad");
            response.setBody(actividad);
            response.addMensajeInfo(wftiResponse.getMensajeSalida());
            response.setSuccessToTrue();
        } catch (WftiException e) {
            response.setSuccessToFalse();
            response.addMensajeError(e.getBusinessMessage());
        }
        return response;
    }
}
