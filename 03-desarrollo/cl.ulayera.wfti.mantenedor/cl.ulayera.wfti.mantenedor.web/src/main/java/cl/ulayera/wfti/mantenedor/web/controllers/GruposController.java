package cl.ulayera.wfti.mantenedor.web.controllers;

import cl.ulayera.wfti.mantenedor.api.exceptions.WftiException;
import cl.ulayera.wfti.mantenedor.api.vo.common.JsonResponse;
import cl.ulayera.wfti.mantenedor.api.vo.common.WftiRequest;
import cl.ulayera.wfti.mantenedor.api.vo.common.WftiResponse;
import cl.ulayera.wfti.mantenedor.api.vo.models.Grupo;
import cl.ulayera.wfti.mantenedor.api.vo.models.Usuario;
import cl.ulayera.wfti.mantenedor.business.services.GrupoService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Creado por ulayera el 11/30/14.
 */
@Controller
public class GruposController {
    private static final Logger LOGGER = Logger.getLogger(LoginController.class);

    @Autowired
    GrupoService grupoService;

    public GruposController() {}

    @RequestMapping(value = "/secure/grupos")
    public ModelAndView ingreso(HttpServletRequest httpServletRequest) {
        LOGGER.debug("/secure/grupos");

        LOGGER.info("Ejecutando: " + new Object(){}.getClass().getName() + "." + new Object(){}.getClass().getEnclosingMethod().getName());
        return new ModelAndView("grupos-view");
    }

    @RequestMapping(value = "/api/grupos/", method = RequestMethod.GET)
    public
    @ResponseBody
    JsonResponse findAllGrupos(HttpServletRequest request) {
        JsonResponse response = new JsonResponse();
        List<Grupo> grupoList;

        WftiRequest wftiRequest = new WftiRequest();
        try {
            WftiResponse wftiResponse = grupoService.findAllGrupos(wftiRequest);
            grupoList = (List<Grupo>) wftiResponse.getResultado("grupos");
            response.setBody(grupoList);
            response.addMensajeInfo(wftiResponse.getMensajeSalida());
            response.setSuccessToTrue();
        } catch (WftiException e) {
            response.setSuccessToFalse();
            response.addMensajeError(e.getBusinessMessage());
        }

        return response;
    }

    @RequestMapping(value = "/api/grupos/{id}/", method = RequestMethod.GET)
    public
    @ResponseBody
    JsonResponse findGrupoById(HttpServletRequest request,
                                 @PathVariable Long id) {
        LOGGER.info("Ejecutando: " + new Object() {
        }.getClass().getName() + "." + new Object() {
        }.getClass().getEnclosingMethod().getName());

        WftiRequest wftiRequest = new WftiRequest();
        JsonResponse response = new JsonResponse();
        Grupo grupo;
        wftiRequest.addParam("id", id);
        try {
            WftiResponse wftiResponse = grupoService.findGrupoById(wftiRequest);
            grupo = (Grupo) wftiResponse.getResultado("grupo");
            response.setBody(grupo);
            response.addMensajeInfo(wftiResponse.getMensajeSalida());
            response.setSuccessToTrue();
        } catch (WftiException e) {
            response.setSuccessToFalse();
            response.addMensajeError(e.getBusinessMessage());
        }

        return response;
    }

    @RequestMapping(value = "/api/grupos/", method = RequestMethod.POST)
    public
    @ResponseBody
    JsonResponse createGrupo(HttpServletRequest request,
                             @RequestBody Grupo grupo) {
        LOGGER.info("Ejecutando: " + new Object() {
        }.getClass().getName() + "." + new Object() {
        }.getClass().getEnclosingMethod().getName());

        WftiRequest wftiRequest = new WftiRequest();
        JsonResponse response = new JsonResponse();
        wftiRequest.addParam("grupo", grupo);
        try {
            WftiResponse wftiResponse = grupoService.createGrupo(wftiRequest);
            grupo = (Grupo) wftiResponse.getResultado("grupo");
            response.setBody(grupo);
            response.addMensajeInfo(wftiResponse.getMensajeSalida());
            response.setSuccessToTrue();
        } catch (WftiException e) {
            response.setSuccessToFalse();
            response.addMensajeError(e.getBusinessMessage());
        }
        return response;
    }

    @RequestMapping(value = "/api/grupos/", method = RequestMethod.DELETE)
    public
    @ResponseBody
    JsonResponse deleteGrupo(HttpServletRequest request,
                             @RequestBody Grupo grupo) {
        LOGGER.info("Ejecutando: " + new Object() {
        }.getClass().getName() + "." + new Object() {
        }.getClass().getEnclosingMethod().getName());

        WftiRequest wftiRequest = new WftiRequest();
        JsonResponse response = new JsonResponse();
        wftiRequest.addParam("grupo", grupo);
        try {
            WftiResponse wftiResponse = grupoService.deleteGrupo(wftiRequest);
            grupo = (Grupo) wftiResponse.getResultado("grupo");
            response.setBody(grupo);
            response.addMensajeInfo(wftiResponse.getMensajeSalida());
            response.setSuccessToTrue();
        } catch (WftiException e) {
            response.setSuccessToFalse();
            response.addMensajeError(e.getBusinessMessage());
        }
        return response;
    }

    @RequestMapping(value = "/api/grupos/", method = RequestMethod.PUT)
    public
    @ResponseBody
    JsonResponse editGrupos(HttpServletRequest request,
                             @RequestBody Grupo grupo) {
        LOGGER.info("Ejecutando: " + new Object() {
        }.getClass().getName() + "." + new Object() {
        }.getClass().getEnclosingMethod().getName());

        WftiRequest wftiRequest = new WftiRequest();
        JsonResponse response = new JsonResponse();
        wftiRequest.addParam("grupo", grupo);
        try {
            WftiResponse wftiResponse = grupoService.editGrupo(wftiRequest);
            response.addMensajeInfo(wftiResponse.getMensajeSalida());
            response.setSuccessToTrue();
        } catch (WftiException e) {
            response.setSuccessToFalse();
            response.addMensajeError(e.getBusinessMessage());
        }
        return response;
    }
}
