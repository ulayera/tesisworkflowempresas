package cl.ulayera.wfti.mantenedor.web.filters;

import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class SecurityFilter implements Filter {

    private static final Logger LOGGER = Logger.getLogger(SecurityFilter.class);

    private FilterConfig filterConfig = null;

    public SecurityFilter() {
    }

    @Override
    public void doFilter(
            ServletRequest request,
            ServletResponse response,
            FilterChain chain) throws IOException, ServletException {

        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;

        boolean ok = false;

        HttpSession httpSession = httpRequest.getSession();

        if (httpSession != null) {
            //valida login
            String token = (String) httpSession.getAttribute("token");
            ok =  token != null;
            //dummy desarrollo
            ok = true;
        }
        if (ok) {
            try {
                chain.doFilter(request, response);
            } catch (Throwable t) {
                LOGGER.error(t.getMessage(), t);
                httpResponse.sendError(HttpServletResponse.SC_FORBIDDEN, "Ha ocurrido un error validando el permiso para acceder a este recurso: \n" + t.getMessage());
            }
        } else {
            LOGGER.error("ooops!, No tiene permiso para acceder a este recurso");
            String urlDestino = httpRequest.getRequestURI();
            httpRequest.getSession().setAttribute("urldestino", urlDestino);
            httpResponse.sendRedirect("/mantenedor/login.htm");
            chain.doFilter(request, response);
        }
    }

    public void destroy() {
    }

    public void init(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
    }
}
