package cl.ulayera.wfti.mantenedor.web.controllers;

import cl.ulayera.wfti.mantenedor.api.enums.CodigoSalidaEnum;
import cl.ulayera.wfti.mantenedor.api.exceptions.WftiException;
import cl.ulayera.wfti.mantenedor.api.vo.common.JsonResponse;
import cl.ulayera.wfti.mantenedor.api.vo.common.WftiRequest;
import cl.ulayera.wfti.mantenedor.api.vo.common.WftiResponse;
import cl.ulayera.wfti.mantenedor.api.vo.models.Usuario;
import cl.ulayera.wfti.mantenedor.business.services.LoginService;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: ulayera
 * Date: 10-11-14
 * Time: 17:03 PM
 */
@Controller
@Scope("session")
public class LoginController {

    private static final Logger LOGGER = Logger.getLogger(LoginController.class);
    private String urlDestino;

    @Autowired
    LoginService loginService;

    public LoginController() {}

    @RequestMapping(value = "/login")
    public ModelAndView ingreso(HttpServletRequest httpServletRequest) {
        LOGGER.debug("/login");
        urlDestino = (String) httpServletRequest.getSession().getAttribute("urldestino");
        LOGGER.info("Ejecutando: " + new Object(){}.getClass().getName() + "." + new Object(){}.getClass().getEnclosingMethod().getName());
        return new ModelAndView("login-view");
    }

    @RequestMapping(value = "/login/intento", method = RequestMethod.POST)
    public
    @ResponseBody
    JsonResponse intentoLogin(HttpServletRequest request,
                              @RequestBody Usuario usuario) {
        LOGGER.debug("/login/intento");
        JsonResponse response = new JsonResponse();

        LOGGER.info(usuario);
        WftiRequest wftiRequest = new WftiRequest();
        wftiRequest.addParam("username",usuario.getUsername());
        wftiRequest.addParam("password",usuario.getPassword());
        WftiResponse wftiResponse;
        try {
            wftiResponse = loginService.intentaLogin(wftiRequest);

            if (CodigoSalidaEnum.OK.equals(wftiResponse.getCodigoSalida())) {
                response.addMensajeInfo("El usuario se identificó correctamente.");
                LOGGER.info("OK");
                response.setSuccessToTrue();
                //response.setBody(usuario);
                request.getSession().setAttribute("token", "tokendummy");
            } else if (CodigoSalidaEnum.NODATA.equals(wftiResponse.getCodigoSalida())) {
                response.addMensajeWarning("El usuario y/o contraseña ingresado son incorrectos.");
                LOGGER.info("NODATA");
                response.setSuccessToFalse();
            } else if (CodigoSalidaEnum.NOOK.equals(wftiResponse.getCodigoSalida())) {
                response.setBody("NOOK");
                LOGGER.info("NOOK");
                response.setSuccessToFalse();
            }

        } catch (WftiException e) {
            LOGGER.error(e);
            response.addMensajeError(e.getBusinessMessage() + ", revise el log para mas información.");
            response.setSuccessToFalse();
        }
        return response;
    }

    public String getUrlDestino() {
        return urlDestino;
    }

    public void setUrlDestino(String urlDestino) {
        this.urlDestino = urlDestino;
    }
}
