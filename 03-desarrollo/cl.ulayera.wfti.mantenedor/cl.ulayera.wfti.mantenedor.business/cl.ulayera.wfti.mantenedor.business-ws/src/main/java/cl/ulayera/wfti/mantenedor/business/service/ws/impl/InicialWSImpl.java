package cl.ulayera.wfti.mantenedor.business.service.ws.impl;

import cl.ulayera.wfti.mantenedor.business.services.InicialEJBService;

import javax.ejb.Stateless;
import javax.jws.WebService;

/**
 * Created by ulayera on 11/4/14.
 */

@WebService
@Stateless
public class InicialWSImpl implements InicialEJBService {

    @Override
    public String sayHi(String name) {
        return "Hi "+ name +"!";
    }
}
