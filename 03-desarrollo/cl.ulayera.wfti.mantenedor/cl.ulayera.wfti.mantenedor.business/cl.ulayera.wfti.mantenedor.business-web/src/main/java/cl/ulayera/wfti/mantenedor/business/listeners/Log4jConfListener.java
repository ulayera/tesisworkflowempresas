package cl.ulayera.wfti.mantenedor.business.listeners;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Created with IntelliJ IDEA.
 * User: ulayera
 * Date: 11/11/14
 * Time: 7:12 PM
 */
public class Log4jConfListener implements ServletContextListener {

    private static final Logger LOGGER = Logger.getLogger(Log4jConfListener.class);

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        String ruta = String.format("%s/wfti/config/log4j-business.properties", System.getProperty("user.dir"));
        try {
            System.out.println("- WFTI Backend logs, ruta: " + ruta);
            System.out.println("- WFTI user dir: " + System.getProperty("user.dir"));
            PropertyConfigurator.configure(ruta);
            LOGGER.info("log4j business configurado para WFTI");
        } catch (Exception e) {
            System.err.println("Error al cargar configuracion de log4j: " + e);
            System.err.println("- Se espera la configuracion bajo la siguiente ruta: " + ruta);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        // do nothing
    }
}
