package cl.ulayera.wfti.mantenedor.business.delegates;

import cl.ulayera.wfti.mantenedor.api.exceptions.WftiException;
import cl.ulayera.wfti.mantenedor.api.utils.ServiceLocator;
import cl.ulayera.wfti.mantenedor.api.vo.common.WftiRequest;
import cl.ulayera.wfti.mantenedor.api.vo.common.WftiResponse;
import cl.ulayera.wfti.mantenedor.business.services.ProyectoService;
import org.apache.log4j.Logger;

/**
 * Creado por ulayera el 12/2/14.
 */
public class ProyectoServiceDelegator implements ProyectoService {
    private static final Logger LOGGER = Logger.getLogger(ProyectoService.class);

    private ProyectoService proyectoService;
    private String jndiName;

    public ProyectoServiceDelegator(String jndiName) {
        this.jndiName = jndiName;
        init();
    }

    private void init() {
        try {
            proyectoService = ServiceLocator.getInstance().lookup(jndiName, ProyectoService.class);
        } catch (Exception e) {
            LOGGER.fatal(e.getMessage(), e);
        }
    }

    @Override
    public WftiResponse findAllProyectos(WftiRequest request) throws WftiException {
        return proyectoService.findAllProyectos(request);
    }

    @Override
    public WftiResponse findProyectoById(WftiRequest request) throws WftiException {
        return proyectoService.findProyectoById(request);
    }

    @Override
    public WftiResponse createProyecto(WftiRequest request) throws WftiException {
        return proyectoService.createProyecto(request);
    }

    @Override
    public WftiResponse deleteProyecto(WftiRequest request) throws WftiException {
        return proyectoService.deleteProyecto(request);
    }

    @Override
    public WftiResponse updateProyecto(WftiRequest request) throws WftiException {
        return proyectoService.updateProyecto(request);
    }

}
