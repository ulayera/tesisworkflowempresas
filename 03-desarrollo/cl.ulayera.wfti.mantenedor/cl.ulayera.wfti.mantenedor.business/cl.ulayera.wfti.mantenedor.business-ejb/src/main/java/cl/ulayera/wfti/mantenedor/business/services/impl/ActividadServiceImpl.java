package cl.ulayera.wfti.mantenedor.business.services.impl;

import cl.ulayera.wfti.mantenedor.api.enums.CodigoSalidaEnum;
import cl.ulayera.wfti.mantenedor.api.exceptions.WftiException;
import cl.ulayera.wfti.mantenedor.api.utils.MySQLDatabaseUtil;
import cl.ulayera.wfti.mantenedor.api.vo.common.WftiRequest;
import cl.ulayera.wfti.mantenedor.api.vo.common.WftiResponse;
import cl.ulayera.wfti.mantenedor.api.vo.models.Actividad;
import cl.ulayera.wfti.mantenedor.business.services.ActividadService;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.util.List;

/**
 * Creado por ulayera el 12/2/14.
 */
@Stateless(name = "ActividadService", mappedName = "ejb/ActividadService")
@Remote(ActividadService.class)
public class ActividadServiceImpl implements ActividadService {
    @Override
    public WftiResponse findAllActividades(WftiRequest request) throws WftiException {
        WftiResponse response = new WftiResponse();
        List<Actividad> actividadList = null;
        try {
            actividadList = MySQLDatabaseUtil.findAllActividades();
        } catch (WftiException e) {
            e.setWftiRequest(request);
            throw e;
        }

        if (actividadList == null || actividadList.isEmpty()){
            response.setCodigoSalida(CodigoSalidaEnum.NODATA);
            response.setMensajeSalida("No se encontraron actividades");
        } else {
            response.setCodigoSalida(CodigoSalidaEnum.OK);
            response.setMensajeSalida("Consulta realizada exitosamente");
            response.addResultado("actividades", actividadList);
        }
        return response;
    }

    @Override
    public WftiResponse findActividadById(WftiRequest request) throws WftiException {
        WftiResponse response = new WftiResponse();
        Actividad actividad = null;
        try {
            actividad = MySQLDatabaseUtil.findActividadById(request.getParam("id", Long.class));
        } catch (WftiException e) {
            e.setWftiRequest(request);
            throw e;
        }

        if (actividad == null){
            response.setCodigoSalida(CodigoSalidaEnum.NODATA);
            response.setMensajeSalida("No se pudo encontrar actividad");
        } else {
            response.setCodigoSalida(CodigoSalidaEnum.OK);
            response.setMensajeSalida("Consulta realizada exitosamente");
            response.addResultado("actividad", actividad);
        }
        return response;
    }

    @Override
    public WftiResponse createActividad(WftiRequest request) throws WftiException {
        WftiResponse response = new WftiResponse();
        Actividad actividad = null;
        try {
            actividad = MySQLDatabaseUtil.createActividad(request.getParam("actividad", Actividad.class));
        } catch (WftiException e) {
            e.setWftiRequest(request);
            throw e;
        }

        if (actividad == null){
            response.setCodigoSalida(CodigoSalidaEnum.NODATA);
            response.setMensajeSalida("No se pudo crear actividad");
        } else {
            response.setCodigoSalida(CodigoSalidaEnum.OK);
            response.setMensajeSalida("Creación realizada exitosamente");
            response.addResultado("actividad", actividad);
        }
        return response;
    }

    @Override
    public WftiResponse deleteActividad(WftiRequest request) throws WftiException {
        WftiResponse response = new WftiResponse();
        if (!MySQLDatabaseUtil.deleteActividad(request.getParam("actividad", Actividad.class))){
            response.setCodigoSalida(CodigoSalidaEnum.NODATA);
            response.setMensajeSalida("No se pudo eliminar actividad");
        } else {
            response.setCodigoSalida(CodigoSalidaEnum.OK);
            response.setMensajeSalida("Eliminación realizada exitosamente");
        }
        return response;
    }

    @Override
    public WftiResponse updateActividad(WftiRequest request) throws WftiException {
        WftiResponse response = new WftiResponse();
        Actividad actividad = null;
        try {
            actividad = MySQLDatabaseUtil.updateActividad(request.getParam("actividad", Actividad.class));
        } catch (WftiException e) {
            e.setWftiRequest(request);
            throw e;
        }

        if (actividad == null){
            response.setCodigoSalida(CodigoSalidaEnum.NODATA);
            response.setMensajeSalida("No se pudo crear actividad");
        } else {
            response.setCodigoSalida(CodigoSalidaEnum.OK);
            response.setMensajeSalida("Creación realizada exitosamente");
            response.addResultado("actividad", actividad);
        }
        return response;
    }
}
