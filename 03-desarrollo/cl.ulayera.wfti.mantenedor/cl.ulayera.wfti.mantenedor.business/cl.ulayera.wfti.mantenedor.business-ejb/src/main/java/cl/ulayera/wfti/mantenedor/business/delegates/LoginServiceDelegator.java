package cl.ulayera.wfti.mantenedor.business.delegates;

import cl.ulayera.wfti.mantenedor.api.exceptions.WftiException;
import cl.ulayera.wfti.mantenedor.api.utils.ServiceLocator;
import cl.ulayera.wfti.mantenedor.api.vo.common.WftiRequest;
import cl.ulayera.wfti.mantenedor.api.vo.common.WftiResponse;
import cl.ulayera.wfti.mantenedor.business.services.LoginService;
import org.apache.log4j.Logger;

import java.io.Serializable;

/**
 * Created by ulayera on 11/12/14.
 */
public class LoginServiceDelegator implements LoginService, Serializable {

    private static final Logger LOGGER = Logger.getLogger(LoginService.class);

    private LoginService loginService;
    private String jndiName;

    public LoginServiceDelegator(String jndiName) {
        this.jndiName = jndiName;
        init();
    }

    private void init() {
        try {
            loginService = ServiceLocator.getInstance().lookup(jndiName, LoginService.class);
        } catch (Exception e) {
            LOGGER.fatal(e.getMessage(), e);
        }
    }

    @Override
    public WftiResponse intentaLogin(WftiRequest request) throws WftiException {
        LOGGER.info("Ejecutando: " + new Object(){}.getClass().getName() + "." + new Object(){}.getClass().getEnclosingMethod().getName());
        return loginService.intentaLogin(request);
    }

    @Override
    public WftiResponse validaSesion(WftiRequest request) {
        return loginService.validaSesion(request);
    }
}
