package cl.ulayera.wfti.mantenedor.business.delegates;

import cl.ulayera.wfti.mantenedor.api.exceptions.WftiException;
import cl.ulayera.wfti.mantenedor.api.utils.ServiceLocator;
import cl.ulayera.wfti.mantenedor.api.vo.common.WftiRequest;
import cl.ulayera.wfti.mantenedor.api.vo.common.WftiResponse;
import cl.ulayera.wfti.mantenedor.business.services.GrupoService;
import org.apache.log4j.Logger;

/**
 * Creado por ulayera el 12/2/14.
 */
public class GrupoServiceDelegator implements GrupoService {
    private static final Logger LOGGER = Logger.getLogger(GrupoService.class);

    private GrupoService grupoService;
    private String jndiName;

    public GrupoServiceDelegator(String jndiName) {
        this.jndiName = jndiName;
        init();
    }

    private void init() {
        try {
            grupoService = ServiceLocator.getInstance().lookup(jndiName, GrupoService.class);
        } catch (Exception e) {
            LOGGER.fatal(e.getMessage(), e);
        }
    }

    @Override
    public WftiResponse findAllGrupos(WftiRequest request) throws WftiException {
        return grupoService.findAllGrupos(request);
    }

    @Override
    public WftiResponse findGrupoById(WftiRequest request) throws WftiException {
        return grupoService.findGrupoById(request);
    }

    @Override
    public WftiResponse createGrupo(WftiRequest request) throws WftiException {
        return grupoService.createGrupo(request);
    }

    @Override
    public WftiResponse deleteGrupo(WftiRequest request) throws WftiException {
        return grupoService.deleteGrupo(request);
    }

    @Override
    public WftiResponse editGrupo(WftiRequest request) throws WftiException {
        return grupoService.editGrupo(request);
    }
}
