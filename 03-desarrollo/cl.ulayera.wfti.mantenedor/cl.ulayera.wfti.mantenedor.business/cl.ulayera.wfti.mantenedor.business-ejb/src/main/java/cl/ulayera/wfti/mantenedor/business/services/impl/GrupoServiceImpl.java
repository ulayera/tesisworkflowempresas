package cl.ulayera.wfti.mantenedor.business.services.impl;

import cl.ulayera.wfti.mantenedor.api.enums.CodigoSalidaEnum;
import cl.ulayera.wfti.mantenedor.api.exceptions.WftiException;
import cl.ulayera.wfti.mantenedor.api.utils.MySQLDatabaseUtil;
import cl.ulayera.wfti.mantenedor.api.vo.common.WftiRequest;
import cl.ulayera.wfti.mantenedor.api.vo.common.WftiResponse;
import cl.ulayera.wfti.mantenedor.api.vo.models.Grupo;
import cl.ulayera.wfti.mantenedor.api.vo.models.Proyecto;
import cl.ulayera.wfti.mantenedor.business.services.GrupoService;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.util.List;

/**
 * Creado por ulayera el 12/2/14.
 */
@Stateless(name = "GrupoService", mappedName = "ejb/GrupoService")
@Remote(GrupoService.class)
public class GrupoServiceImpl implements GrupoService {
    @Override
    public WftiResponse findAllGrupos(WftiRequest request) throws WftiException {
        WftiResponse response = new WftiResponse();
        List<Grupo> grupoList = null;
        try {
            grupoList = MySQLDatabaseUtil.findAllGrupos();
        } catch (WftiException e) {
            e.setWftiRequest(request);
            throw e;
        }

        if (grupoList == null || grupoList.isEmpty()){
            response.setCodigoSalida(CodigoSalidaEnum.NODATA);
            response.setMensajeSalida("No se encontraron grupos");
        } else {
            response.setCodigoSalida(CodigoSalidaEnum.OK);
            response.setMensajeSalida("Consulta realizada exitosamente");
            response.addResultado("grupos", grupoList);
        }
        return response;
    }

    @Override
    public WftiResponse findGrupoById(WftiRequest request) throws WftiException {
        WftiResponse response = new WftiResponse();
        Grupo grupo = null;
        try {
            grupo = MySQLDatabaseUtil.findGrupoById(request.getParam("id", Long.class));
        } catch (WftiException e) {
            e.setWftiRequest(request);
            throw e;
        }

        if (grupo == null){
            response.setCodigoSalida(CodigoSalidaEnum.NODATA);
            response.setMensajeSalida("No se pudo encontrar grupo");
        } else {
            response.setCodigoSalida(CodigoSalidaEnum.OK);
            response.setMensajeSalida("Consulta realizada exitosamente");
            response.addResultado("grupo", grupo);
        }
        return response;
    }

    @Override
    public WftiResponse createGrupo(WftiRequest request) throws WftiException {
        WftiResponse response = new WftiResponse();
        Grupo grupo = null;
        try {
            grupo = MySQLDatabaseUtil.createGrupo(request.getParam("grupo", Grupo.class));
        } catch (WftiException e) {
            e.setWftiRequest(request);
            throw e;
        }

        if (grupo == null){
            response.setCodigoSalida(CodigoSalidaEnum.NODATA);
            response.setMensajeSalida("No se pudo crear grupo");
        } else {
            response.setCodigoSalida(CodigoSalidaEnum.OK);
            response.setMensajeSalida("Creación realizada exitosamente");
            response.addResultado("grupo", grupo);
        }
        return response;
    }

    @Override
    public WftiResponse deleteGrupo(WftiRequest request) throws WftiException {
        WftiResponse response = new WftiResponse();
        if (!MySQLDatabaseUtil.deleteGrupo(request.getParam("grupo", Grupo.class))){
            response.setCodigoSalida(CodigoSalidaEnum.NODATA);
            response.setMensajeSalida("No se pudo crear grupo");
        } else {
            response.setCodigoSalida(CodigoSalidaEnum.OK);
            response.setMensajeSalida("Eliminación realizada exitosamente");
        }
        return response;
    }

    @Override
    public WftiResponse editGrupo(WftiRequest request) throws WftiException {
        WftiResponse response = new WftiResponse();
        if (!MySQLDatabaseUtil.editGrupo(request.getParam("grupo", Grupo.class))){
            response.setCodigoSalida(CodigoSalidaEnum.NODATA);
            response.setMensajeSalida("No se pudo editar grupo");
        } else {
            response.setCodigoSalida(CodigoSalidaEnum.OK);
            response.setMensajeSalida("Edición realizada exitosamente");
        }
        return response;
    }
}
