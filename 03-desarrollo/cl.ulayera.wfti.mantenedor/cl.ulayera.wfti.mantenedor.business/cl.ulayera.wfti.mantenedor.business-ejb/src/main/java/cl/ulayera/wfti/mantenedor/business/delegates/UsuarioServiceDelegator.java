package cl.ulayera.wfti.mantenedor.business.delegates;

import cl.ulayera.wfti.mantenedor.api.exceptions.WftiException;
import cl.ulayera.wfti.mantenedor.api.utils.ServiceLocator;
import cl.ulayera.wfti.mantenedor.api.vo.common.WftiRequest;
import cl.ulayera.wfti.mantenedor.api.vo.common.WftiResponse;
import cl.ulayera.wfti.mantenedor.business.services.UsuarioService;
import org.apache.log4j.Logger;

/**
 * Creado por ulayera el 12/2/14.
 */
public class UsuarioServiceDelegator implements UsuarioService {
    private static final Logger LOGGER = Logger.getLogger(UsuarioService.class);

    private UsuarioService usuarioService;
    private String jndiName;

    public UsuarioServiceDelegator(String jndiName) {
        this.jndiName = jndiName;
        init();
    }

    private void init() {
        try {
            usuarioService = ServiceLocator.getInstance().lookup(jndiName, UsuarioService.class);
        } catch (Exception e) {
            LOGGER.fatal(e.getMessage(), e);
        }
    }

    @Override
    public WftiResponse findAllUsuarios(WftiRequest request) throws WftiException {
        return usuarioService.findAllUsuarios(request);
    }

    @Override
    public WftiResponse findUsuarioById(WftiRequest request) throws WftiException {
        return usuarioService.findUsuarioById(request);
    }

    @Override
    public WftiResponse createUsuario(WftiRequest request) throws WftiException {
        return usuarioService.createUsuario(request);
    }

    @Override
    public WftiResponse deleteUsuario(WftiRequest request) throws WftiException {
        return usuarioService.deleteUsuario(request);
    }

    @Override
    public WftiResponse editUsuario(WftiRequest request) throws WftiException {
        return usuarioService.editUsuario(request);
    }
}
