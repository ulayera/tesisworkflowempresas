package cl.ulayera.wfti.mantenedor.business.services.impl;

import cl.ulayera.wfti.mantenedor.api.enums.CodigoSalidaEnum;
import cl.ulayera.wfti.mantenedor.api.exceptions.WftiException;
import cl.ulayera.wfti.mantenedor.api.utils.MySQLDatabaseUtil;
import cl.ulayera.wfti.mantenedor.api.vo.common.WftiRequest;
import cl.ulayera.wfti.mantenedor.api.vo.common.WftiResponse;
import cl.ulayera.wfti.mantenedor.api.vo.models.Usuario;
import cl.ulayera.wfti.mantenedor.business.services.UsuarioService;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.util.List;

/**
 * Creado por ulayera el 12/2/14.
 */
@Stateless(name = "UsuarioService", mappedName = "ejb/UsuarioService")
@Remote(UsuarioService.class)
public class UsuarioServiceImpl implements UsuarioService {

    @Override
    public WftiResponse findAllUsuarios(WftiRequest request) throws WftiException {
        WftiResponse response = new WftiResponse();
        List<Usuario> usuarioList = null;
        try {
            usuarioList = MySQLDatabaseUtil.findAllUsuarios();
        } catch (WftiException e) {
            e.setWftiRequest(request);
            throw e;
        }

        if (usuarioList == null || usuarioList.isEmpty()){
            response.setCodigoSalida(CodigoSalidaEnum.NODATA);
            response.setMensajeSalida("No se encontraron usuarios");
        } else {
            response.setCodigoSalida(CodigoSalidaEnum.OK);
            response.setMensajeSalida("Consulta realizada exitosamente");
            response.addResultado("usuarios", usuarioList);
        }
        return response;
    }

    @Override
    public WftiResponse findUsuarioById(WftiRequest request) throws WftiException {
        WftiResponse response = new WftiResponse();
        Usuario usuario = null;
        try {
            usuario = MySQLDatabaseUtil.findUsuarioById(request.getParam("id", Long.class));
        } catch (WftiException e) {
            e.setWftiRequest(request);
            throw e;
        }

        if (usuario == null){
            response.setCodigoSalida(CodigoSalidaEnum.NODATA);
            response.setMensajeSalida("No se encontraron usuarios");
        } else {
            response.setCodigoSalida(CodigoSalidaEnum.OK);
            response.setMensajeSalida("Consulta realizada exitosamente");
            response.addResultado("usuario", usuario);
        }
        return response;
    }

    @Override
    public WftiResponse createUsuario(WftiRequest request) throws WftiException {
        WftiResponse response = new WftiResponse();
        Usuario usuario = null;
        try {
            usuario = MySQLDatabaseUtil.createUsuario(request.getParam("usuario", Usuario.class));
        } catch (WftiException e) {
            e.setWftiRequest(request);
            throw e;
        }

        if (usuario == null){
            response.setCodigoSalida(CodigoSalidaEnum.NODATA);
            response.setMensajeSalida("No se pudo crear usuario");
        } else {
            response.setCodigoSalida(CodigoSalidaEnum.OK);
            response.setMensajeSalida("Creación realizada exitosamente");
            response.addResultado("usuario", usuario);
        }
        return response;
    }

    @Override
    public WftiResponse deleteUsuario(WftiRequest request) throws WftiException {
        WftiResponse response = new WftiResponse();
        if (!MySQLDatabaseUtil.deleteUsuario(request.getParam("usuario", Usuario.class))){
            response.setCodigoSalida(CodigoSalidaEnum.NODATA);
            response.setMensajeSalida("No se pudo eliminar usuario");
        } else {
            response.setCodigoSalida(CodigoSalidaEnum.OK);
            response.setMensajeSalida("Eliminación realizada exitosamente");
        }
        return response;
    }

    @Override
    public WftiResponse editUsuario(WftiRequest request) throws WftiException {
        WftiResponse response = new WftiResponse();
        if (!MySQLDatabaseUtil.editUsuario(request.getParam("usuario", Usuario.class))){
            response.setCodigoSalida(CodigoSalidaEnum.NODATA);
            response.setMensajeSalida("No se pudo editar usuario");
        } else {
            response.setCodigoSalida(CodigoSalidaEnum.OK);
            response.setMensajeSalida("Edición realizada exitosamente");
        }
        return response;
    }
}
