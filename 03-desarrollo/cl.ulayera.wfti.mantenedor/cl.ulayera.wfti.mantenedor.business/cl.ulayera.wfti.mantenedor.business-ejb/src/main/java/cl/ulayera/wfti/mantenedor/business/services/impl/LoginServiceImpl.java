package cl.ulayera.wfti.mantenedor.business.services.impl;

import cl.ulayera.wfti.mantenedor.api.enums.CodigoSalidaEnum;
import cl.ulayera.wfti.mantenedor.api.exceptions.WftiException;
import cl.ulayera.wfti.mantenedor.api.utils.MySQLDatabaseUtil;
import cl.ulayera.wfti.mantenedor.api.vo.common.WftiRequest;
import cl.ulayera.wfti.mantenedor.api.vo.common.WftiResponse;
import cl.ulayera.wfti.mantenedor.api.vo.models.Proyecto;
import cl.ulayera.wfti.mantenedor.api.vo.models.Usuario;
import cl.ulayera.wfti.mantenedor.business.services.LoginService;
import org.apache.log4j.Logger;

import javax.ejb.Remote;
import javax.ejb.Stateless;

/**
 * Created by ulayera on 11/11/14.
 */

@Stateless(name = "LoginService", mappedName = "ejb/LoginService")
@Remote(LoginService.class)
public class LoginServiceImpl implements LoginService {
    private static final Logger LOGGER = Logger.getLogger(LoginServiceImpl.class);


    @Override
    public WftiResponse intentaLogin(WftiRequest request) throws WftiException {
        WftiResponse response = new WftiResponse();
        String username = (String) request.getParam("username");
        String password = (String) request.getParam("password");

        Usuario usuario = null;
        try {
            usuario = MySQLDatabaseUtil.findUsuarioByUsername(username);
        } catch (WftiException e) {
            e.setWftiRequest(request);
            throw e;
        }

        if (usuario == null){
            response.setCodigoSalida(CodigoSalidaEnum.NODATA);
            response.setMensajeSalida("Usuario no existe");
        } else if (!usuario.getPassword().equals(password)) {
            response.setCodigoSalida(CodigoSalidaEnum.NOOK);
            response.setMensajeSalida("Contraseña inválida");
        } else {
            response.setCodigoSalida(CodigoSalidaEnum.OK);
            response.setMensajeSalida("Usuario y contraseña correctos");
        }
        return response;
    }

    @Override
    public WftiResponse validaSesion(WftiRequest request) {
        return null;
    }

}
