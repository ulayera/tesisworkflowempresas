package cl.ulayera.wfti.mantenedor.business.services.impl;

import cl.ulayera.wfti.mantenedor.api.enums.CodigoSalidaEnum;
import cl.ulayera.wfti.mantenedor.api.exceptions.WftiException;
import cl.ulayera.wfti.mantenedor.api.utils.MySQLDatabaseUtil;
import cl.ulayera.wfti.mantenedor.api.vo.common.WftiRequest;
import cl.ulayera.wfti.mantenedor.api.vo.common.WftiResponse;
import cl.ulayera.wfti.mantenedor.api.vo.models.Proyecto;
import cl.ulayera.wfti.mantenedor.business.services.ProyectoService;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.util.List;

/**
 * Creado por ulayera el 12/2/14.
 */
@Stateless(name = "ProyectoService", mappedName = "ejb/ProyectoService")
@Remote(ProyectoService.class)
public class ProyectoServiceImpl implements ProyectoService {
    @Override
    public WftiResponse findAllProyectos(WftiRequest request) throws WftiException {
        WftiResponse response = new WftiResponse();
        List<Proyecto> proyectoList = null;
        try {
            proyectoList = MySQLDatabaseUtil.findAllProyectos();
        } catch (WftiException e) {
            e.setWftiRequest(request);
            throw e;
        }

        if (proyectoList == null || proyectoList.isEmpty()){
            response.setCodigoSalida(CodigoSalidaEnum.NODATA);
            response.setMensajeSalida("No se encontraron proyectos");
        } else {
            response.setCodigoSalida(CodigoSalidaEnum.OK);
            response.setMensajeSalida("Consulta realizada exitosamente");
            response.addResultado("proyectos", proyectoList);
        }
        return response;
    }

    @Override
    public WftiResponse findProyectoById(WftiRequest request) throws WftiException {
        WftiResponse response = new WftiResponse();
        Proyecto proyecto = null;
        try {
            proyecto = MySQLDatabaseUtil.findProyectoById(request.getParam("id", Long.class));
        } catch (WftiException e) {
            e.setWftiRequest(request);
            throw e;
        }

        if (proyecto == null){
            response.setCodigoSalida(CodigoSalidaEnum.NODATA);
            response.setMensajeSalida("No se pudo encontrar proyecto");
        } else {
            response.setCodigoSalida(CodigoSalidaEnum.OK);
            response.setMensajeSalida("Consulta realizada exitosamente");
            response.addResultado("proyecto", proyecto);
        }
        return response;
    }

    @Override
    public WftiResponse createProyecto(WftiRequest request) throws WftiException {
        WftiResponse response = new WftiResponse();
        Proyecto proyecto = null;
        try {
            proyecto = MySQLDatabaseUtil.createProyecto(request.getParam("proyecto", Proyecto.class));
        } catch (WftiException e) {
            e.setWftiRequest(request);
            throw e;
        }

        if (proyecto == null){
            response.setCodigoSalida(CodigoSalidaEnum.NODATA);
            response.setMensajeSalida("No se pudo crear proyecto");
        } else {
            response.setCodigoSalida(CodigoSalidaEnum.OK);
            response.setMensajeSalida("Creación realizada exitosamente");
            response.addResultado("proyecto", proyecto);
        }
        return response;
    }

    @Override
    public WftiResponse updateProyecto(WftiRequest request) throws WftiException {
        WftiResponse response = new WftiResponse();
        Proyecto proyecto = null;
        try {
            proyecto = MySQLDatabaseUtil.updateProyecto(request.getParam("proyecto", Proyecto.class));
        } catch (WftiException e) {
            e.setWftiRequest(request);
            throw e;
        }

        if (proyecto == null){
            response.setCodigoSalida(CodigoSalidaEnum.NODATA);
            response.setMensajeSalida("No se pudo actualizar proyecto");
        } else {
            response.setCodigoSalida(CodigoSalidaEnum.OK);
            response.setMensajeSalida("Actualización realizada exitosamente");
            response.addResultado("proyecto", proyecto);
        }
        return response;
    }



    @Override
    public WftiResponse deleteProyecto(WftiRequest request) throws WftiException {
        WftiResponse response = new WftiResponse();
        if (!MySQLDatabaseUtil.deleteProyecto(request.getParam("proyecto", Proyecto.class))){
            response.setCodigoSalida(CodigoSalidaEnum.NODATA);
            response.setMensajeSalida("No se pudo eliminar proyecto");
        } else {
            response.setCodigoSalida(CodigoSalidaEnum.OK);
            response.setMensajeSalida("Eliminación realizada exitosamente");
        }
        return response;
    }
}
