package cl.ulayera.wfti.mantenedor.business.delegates;

import cl.ulayera.wfti.mantenedor.api.exceptions.WftiException;
import cl.ulayera.wfti.mantenedor.api.utils.ServiceLocator;
import cl.ulayera.wfti.mantenedor.api.vo.common.WftiRequest;
import cl.ulayera.wfti.mantenedor.api.vo.common.WftiResponse;
import cl.ulayera.wfti.mantenedor.business.services.ActividadService;
import org.apache.log4j.Logger;

/**
 * Creado por ulayera el 12/2/14.
 */
public class ActividadServiceDelegator implements ActividadService {
    private static final Logger LOGGER = Logger.getLogger(ActividadService.class);

    private ActividadService actividadService;
    private String jndiName;

    public ActividadServiceDelegator(String jndiName) {
        this.jndiName = jndiName;
        init();
    }

    private void init() {
        try {
            actividadService = ServiceLocator.getInstance().lookup(jndiName, ActividadService.class);
        } catch (Exception e) {
            LOGGER.fatal(e.getMessage(), e);
        }
    }

    @Override
    public WftiResponse findAllActividades(WftiRequest request) throws WftiException {
        return actividadService.findAllActividades(request);
    }

    @Override
    public WftiResponse findActividadById(WftiRequest request) throws WftiException {
        return actividadService.findActividadById(request);
    }

    @Override
    public WftiResponse createActividad(WftiRequest request) throws WftiException {
        return actividadService.createActividad(request);
    }

    @Override
    public WftiResponse deleteActividad(WftiRequest request) throws WftiException {
        return actividadService.deleteActividad(request);
    }

    @Override
    public WftiResponse updateActividad(WftiRequest request) throws WftiException {
        return actividadService.updateActividad(request);
    }
}
