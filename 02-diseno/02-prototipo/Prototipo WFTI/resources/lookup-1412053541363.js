(function(window, undefined) {
  var dictionary = {
    "d8f6a7bf-a413-411c-a737-5d945a067484": "Reporte por Responsable",
    "f568bf02-7e21-4fa9-90d8-bae43c57e84d": "Editar Actividad",
    "f4ccd842-f8d8-4cf9-a373-929d1f4a3842": "Proyectos",
    "d12245cc-1680-458d-89dd-4f0d7fb22724": "Main",
    "f114e6a4-929d-487c-a368-4b6c72c868bf": "Actividades",
    "1a7ca2f6-ddbb-4fc2-92fb-9400a609cac2": "Usuarios",
    "23e3d332-8ab4-459d-9792-abb9fb9d4846": "Editar Grupo",
    "ecc17d11-9860-486d-aa17-57d548d4a76d": "Login",
    "032d11ff-99cb-4d54-9f91-dbc43611e3ad": "Editar Usuario",
    "c4a4e517-11f8-4ea6-bc0a-604f7fa12754": "Editar Proyecto",
    "340cc6e5-002e-4481-ab16-7021c55fd09a": "Reporte por Proyecto",
    "29bb465b-25bb-4004-b8d1-0fca47f4fb7d": "Grupos",
    "87db3cf7-6bd4-40c3-b29c-45680fb11462": "960 grid - 16 columns",
    "e5f958a4-53ae-426e-8c05-2f7d8e00b762": "960 grid - 12 columns",
    "f39803f7-df02-4169-93eb-7547fb8c961a": "Template 1"
  };

  var uriRE = /^(\/#)?(screens|templates|masters)\/(.*)(\.html)?/;
  window.lookUpURL = function(fragment) {
    var matches = uriRE.exec(fragment || "") || [],
        folder = matches[2] || "",
        canvas = matches[3] || "",
        name, url;
    if(dictionary.hasOwnProperty(canvas)) { /* search by name */
      url = folder + "/" + canvas;
    }
    return url;
  };

  window.lookUpName = function(fragment) {
    var matches = uriRE.exec(fragment || "") || [],
        folder = matches[2] || "",
        canvas = matches[3] || "",
        name, canvasName;
    if(dictionary.hasOwnProperty(canvas)) { /* search by name */
      canvasName = dictionary[canvas];
    }
    return canvasName;
  };
})(window);